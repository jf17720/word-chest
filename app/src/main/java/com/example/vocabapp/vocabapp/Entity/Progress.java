package com.example.vocabapp.vocabapp.Entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Progress {

    private Integer id;

    private String word_Name;

    private String initial_sound;

    private String rhyme;

    private String association;

    private String sentence;

    private String category;

    private String photoPath;

    private String audioPath;

    private String lookAndFeel;

    private String videoPath;

    private String wordAudioPath;

    private Integer syllableFinished;

    private Integer studentId;

    private String topic;

    private Integer topicId;

    private Integer teacherId;

    private Integer complete;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setWord_Name(String word_Name) {
        this.word_Name = word_Name;
    }

    public void setInitial_sound(String initial_sound) {
        this.initial_sound = initial_sound;
    }

    public void setRhyme(String rhyme) {
        this.rhyme = rhyme;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public void setWordAudioPath(String wordAudioPath) {
        this.wordAudioPath = wordAudioPath;
    }

    public void setSyllableFinished(Integer syllableFinished) {
        this.syllableFinished = syllableFinished;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public void setComplete(Integer complete) {
        this.complete = complete;
    }

    public Integer getId() {
        return id;
    }

    public String getWord_Name() {
        return word_Name;
    }

    public String getInitial_sound() {
        return initial_sound;
    }

    public String getRhyme() {
        return rhyme;
    }

    public String getAssociation() {
        return association;
    }

    public String getSentence() {
        return sentence;
    }

    public String getCategory() {
        return category;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public String getLookAndFeel() {
        return lookAndFeel;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public String getWordAudioPath() {
        return wordAudioPath;
    }

    public Integer getSyllableFinished() {
        return syllableFinished;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public String getTopic() {
        return topic;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public Integer getComplete() {
        return complete;
    }

    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", word='" + word_Name + '\'' +
                ", topic='" + topic + '\'' +
                ", rhyme='" + rhyme + '\'' +
                ", association='" + association + '\'' +
                ", sentence='" + sentence + '\'' +
                ", category='" + category + '\'' +
                ", look and feel='" + lookAndFeel + '\'' +
                ", photoPath='" + photoPath + '\'' +
                ", audioPath='" + audioPath + '\'' +
                ", videoPath='" + videoPath + '\'' +
                ", wordAudioPath='" + wordAudioPath + '\'' +
                ", syllableFinished='" + syllableFinished + '\'' +
                '}';
    }

    //Convert from json object to java object
    public static Progress fromJson(JSONObject jsonObject) {
        Progress progress = new Progress();
        try {
            progress.id = jsonObject.getInt("id");
            progress.word_Name = jsonObject.getString("name");
            progress.rhyme = jsonObject.getString("rhyme");
            progress.association = jsonObject.getString("association");
            progress.sentence = jsonObject.getString("sentence");
            progress.category = jsonObject.getString("category");
            progress.lookAndFeel = jsonObject.getString("lookAndFeel");
            progress.syllableFinished = jsonObject.getInt("syllableFinished");
            progress.studentId = jsonObject.getInt("studentId");
            progress.topic = jsonObject.getString("topic");
            progress.topicId = jsonObject.getInt("topicId");
            progress.teacherId = jsonObject.getInt("teacherId");
            progress.complete = jsonObject.getInt("completed");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return progress;
    }

    //Convert from json array to java array of objects
    public static ArrayList<Progress> fromJson(JSONArray jsonArray) {
        JSONObject progressJson;
        ArrayList<Progress> progresses = new ArrayList<Progress>(jsonArray.length());

        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                progressJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Progress progress = Progress.fromJson(progressJson);
            if(progress != null) {
                progresses.add(progress);
            }
        }
        return progresses;

    }


}

