package com.example.vocabapp.vocabapp.Tools;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.vocabapp.vocabapp.Entity.Progress;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.LearnWordActivity.Sentence;
import com.example.vocabapp.vocabapp.R;

import java.util.ArrayList;

public class WordProgressAdaptor extends BaseAdapter{

    private ArrayList<Word> words;
    private ArrayList<Progress> progresses;
    private LayoutInflater mInflater;
    private Integer studentId;
    private Word word;
    Boolean flag = false;

    public WordProgressAdaptor(LayoutInflater inflater, ArrayList<Word> words, ArrayList<Progress> progresses, Integer studentId){

        this.mInflater = inflater;
        this.words = words;
        this.progresses = progresses;
        this.studentId = studentId;

    }

    @Override
    public int getCount() {
        return words.size();
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int i) {

        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View progressView = mInflater.inflate(R.layout.custom_layout3, null);
        word = words.get(position);

        //Getting each item from the layout
        TextView textView = progressView.findViewById(R.id.Custom3TextView_Main);
        ImageView imageView = progressView.findViewById(R.id.Custom3ImageView_Coin);
        textView.setText(word.getName());

        //defaut color of the coin: grey
        imageView.setBackgroundResource(R.drawable.coingrey);
        imageView.setVisibility(View.VISIBLE);


        //light up the grey if completed
        for (Progress progress : progresses) {
            if (progress.getWord_Name().equalsIgnoreCase(word.getName())) {
                if (progress.getComplete() == 1) {
                    imageView.setBackgroundResource(R.drawable.coin);
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        }

        return progressView;
    }
}

