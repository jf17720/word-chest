package com.example.vocabapp.vocabapp.Tools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vocabapp.vocabapp.Entity.Progress;
import com.example.vocabapp.vocabapp.Entity.Student;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.R;

import java.util.ArrayList;

public class TopicProgressAdaptor extends BaseAdapter {

    private ArrayList<Topic> topics;
    private ArrayList<Progress> progresses;
    private LayoutInflater mInflater;

    public TopicProgressAdaptor(LayoutInflater inflater, ArrayList<Topic> topics){

        this.mInflater = inflater;
        this.topics = topics;
//        this.progresses = progresses;

    }

    @Override
    public int getCount() {
        return topics.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        Topic topic = topics.get(position);
        //get the view
        View progressView = mInflater.inflate(R.layout.custom_layout2,null);

        //Getting each item from the layout
        TextView textView_Main = progressView.findViewById(R.id.Custom2TextView_Main);
        TextView textView_Progress = progressView.findViewById(R.id.Custom2_Progress);

        //the number of word completed in this topic
//        for(Progress progress : progresses) {
//            if(progress.getTopicId() == topic.getId()) {
//                if(progress.getComplete() == 1) { //1 means the word is completed, 0 means not
//                    num++;
//                }
//            }
//        }
        textView_Main.setText(topic.getName().trim());//show the topic name
//        String show = String.valueOf(num) + "/" + topic.getWordCount(); //e.g: 2/10
//        textView_Progress.setText(show);
//        textView_Progress.setVisibility(View.VISIBLE);

        return progressView;
    }

}
