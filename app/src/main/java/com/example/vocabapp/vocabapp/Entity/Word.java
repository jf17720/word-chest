package com.example.vocabapp.vocabapp.Entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Word {

    private Integer id;
    private Integer std_id;
    private String name;

    private String syllable;

    private String initial_sound;

    private Integer topicId;
    private Integer teacherId;

    private String topic;
    private String rhyme;
    private String association;
    private String sentence;
    private String category;
    private String photoPath;
    private String audioPath;
    private String lookAndFeel;
    private String videoPath;
    private String wordAudioPath;
    private Integer syllableFinished;


    public Word(Integer id,Integer studentId, String name,String syllable,Integer topicId,Integer teacherId,
                String rhyme, String sentence) {
        this.id = id;
        this.std_id = studentId;
        this.name = name;
        this.syllable = syllable;
        this.topicId = topicId;
        this.teacherId = teacherId;
        this.rhyme = rhyme;
        this.sentence = sentence;
    }

    public Word(){}

    public void setId(Integer id) {

        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSyllable(String syllable) {
        this.syllable = syllable;
    }

    public void setInitial_sound(String initial_sound) {
        this.initial_sound = initial_sound;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public String getSyllable() {
        return syllable;
    }

    public String getInitial_sound() {
        return initial_sound;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public Integer getId() {
        return id;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    //Local database
    public  String getRhyme() {
        return rhyme;
    }

    public void setRhyme(String rhyme) {
        this.rhyme = rhyme;
    }

    public  String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public  String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public  String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public  String getLookAndFeel() {
        return lookAndFeel;
    }

    public  void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    public  String getPhotoPath() {
        return photoPath;
    }

    public  void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public  String getAudioPath() {
        return audioPath;
    }

    public  void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public  String getTopic() {
        return topic;
    }

    public  void setTopic(String topic) {
        this.topic = topic;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getWordAudioPath() {
        return wordAudioPath;
    }

    public void setWordAudioPath(String wordAudioPath) {
        this.wordAudioPath = wordAudioPath;
    }

    public Integer getSyllableFinished() {
        return syllableFinished;
    }

    public void setSyllableFinished(Integer syllableFinished) {
        this.syllableFinished = syllableFinished;
    }

    public Integer getStd_id() {
        return std_id;
    }

    public void setStd_id(Integer std_id) {
        this.std_id = std_id;
    }

    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", std_id='"+ std_id + '\'' +
                ", word='" + name + '\'' +
                ", topic='" + topic + '\'' +
                ", rhyme='" + rhyme + '\'' +
                ", association='" + association + '\'' +
                ", sentence='" + sentence + '\'' +
                ", category='" + category + '\'' +
                ", look and feel='" + lookAndFeel + '\'' +
                ", photoPath='" + photoPath + '\'' +
                ", audioPath='" + audioPath + '\'' +
                ", videoPath='" + videoPath + '\'' +
                ", wordAudioPath='" + wordAudioPath + '\'' +
                ", syllableFinished='" + syllableFinished + '\'' +
                '}';
    }


    //Convert from json object to java object
    public static Word fromJson(JSONObject jsonObject) {
        Word word = new Word();

        try {
            word.id = jsonObject.getInt("id");
            word.name = jsonObject.getString("name");
            word.syllable = jsonObject.getString("syllable");
            word.topicId = jsonObject.getInt("topicId");
            word.teacherId = jsonObject.getInt("teacherId");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return word;
    }


    //Convert from json array to java array of objects
    public static ArrayList<Word> fromJson(JSONArray jsonArray) {
        JSONObject wordJson;
        ArrayList<Word> words = new ArrayList<Word>(jsonArray.length());

        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                wordJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Word word = Word.fromJson(wordJson);
            if(word != null) {
                words.add(word);
            }
        }
        return words;

    }
}
