package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.TextView;
import	android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;


public class WriteCategory extends AppCompatActivity {
    private String content;
    public static int CategoryVisible = 0;
    private MyDatabaseHelper dbHelper;
    private String currentCategory = "";
    private Boolean ifInputExisted = false;
    private String BASIC_URL;
    private TextView showWord;
    private String theWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_category);

        Intent intent = getIntent();
        theWord = intent.getStringExtra("the_word");
        showWord = findViewById(R.id.showWord_category);
        showWord.setText(theWord);
        final TextView categoryInput = (TextView) findViewById(R.id.EditTextCategory);
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        BASIC_URL = getURL();
        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()});

        if(cursor.moveToFirst()){
            do{
                currentCategory = cursor.getString(cursor.getColumnIndex("category"));
                if(currentCategory != null) categoryInput.setText(currentCategory);
                if(currentCategory == null) ifInputExisted = !ifInputExisted;
            } while (cursor.moveToNext());
            cursor.close();
        }

        String hint = String.format("What other categories could %s fit in?",theWord);
        categoryInput.setHint(hint);

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromWriteCategory);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(WriteCategory.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromWriteCategory);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(WriteCategory.this, Student_Home.class);
                startActivity(intent);
            }

        });

        //when the button is clicked get the text from the box and save it, produce a message saying that it has been saved.
        Button saveButton = (Button) findViewById(R.id.SaveButtonWriteCategory);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                get what the user has put in the box
                content = categoryInput.getText().toString();
//              if they havent written anything then let user know that input cannot be left empty
                if (content.matches("")){
                    Toast.makeText(WriteCategory.this, "INPUT CANNOT BE EMPTY", Toast.LENGTH_SHORT).show();
                }else {
//                    if input is not empty then save it
                    Toast.makeText(WriteCategory.this, "SAVED!", Toast.LENGTH_SHORT).show();
                    final CRUD_interface word = new DatabaseController_Word( v.getContext(),"WORD_STORE.db", null,1);
                    addToOnlineDatabase(content);
                        Word sele = word.select(theWord,getStudentID());
                        sele.setCategory(content);
                        word.update(sele);
                        Log.d("Category","updated:"+ sele.toString());
//                  change the icon to be colourful
                    CategoryVisible = 1;
                    if(CategoryVisible != 1) {
                        Learnwords.yellow++;
                    }
//                    return the user to the learn words page
                    Intent intent = new Intent(WriteCategory.this, Learnwords.class);
                    startActivity(intent);

                }

            }

        });




    }
    private Integer getStudentID() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        final String Student_NAME = "Key_StudentName";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }

    protected String addToOnlineDatabase(String updatedCategory) {
        Integer StudentID = getStudentID();
        String URL = BASIC_URL + "progress/category?wordName=" + theWord + "&" + "studentId=" + StudentID + "&" + "category=" + updatedCategory;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT,URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("Failed")) {
                        } else {
                            Toast.makeText(WriteCategory.this, "added successfully!", Toast.LENGTH_SHORT).show();
//                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WriteCategory.this,"Request failed",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(WriteCategory.this);
        queue.add(stringRequest);
        return null;
    }

    private String getURL() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL,null);
        return url;
    }

}
