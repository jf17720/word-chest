package com.example.vocabapp.vocabapp.LocalDB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.security.AccessControlContext;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    public static final String CREATE_WORD = "create table Word ("   // SQL command for creating a table
            + "id integer primary key autoincrement, "
            + "std_id integer, "
            + "word text, "
            + "topic text, "
            + "rhyme text, "
            + "association text, "
            + "sentence text, "
            + "category text, "
            + "lookAndFeel text, "
            + "photoPath text, "
            + "audioPath text, "
            + "videoPath text, "
            + "wordAudioPath text, "
            + "syllableFinished integer )";


    public Context mContext;


    public MyDatabaseHelper( Context context,  String name,  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        mContext = context;
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_WORD); //creating the table
        Toast.makeText(mContext, "Created successfully", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists Word");
        onCreate(db);
    }
}
