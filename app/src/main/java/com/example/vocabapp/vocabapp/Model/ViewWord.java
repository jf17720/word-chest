package com.example.vocabapp.vocabapp.Model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.gesture.GestureOverlayView;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;
import com.example.vocabapp.vocabapp.TeacherAddTopic;
import com.example.vocabapp.vocabapp.TeacherAddWord;
import com.example.vocabapp.vocabapp.Teacher_Home;


import org.json.JSONArray;

import java.util.ArrayList;

public class ViewWord extends AppCompatActivity {

    SharedPreferences userInfo,topicInfo;
    private static final String Teacher_ID = "Key_TeacherId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    private static final String TAG = "ViewWord";

    private SwipeMenuListView listView;
    private ImageButton goBack;
    private ImageView toHome,addWord;
    private ArrayList<Word> words;
    private Integer wordCountInTopic;

    private static final String TOPIC_ID = "Key_TopicId";
    private static final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";

    MyDatabaseHelper dbHelper;
    ArrayAdapter<String> adapter;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_word);
        listView = findViewById(R.id.ViewWord_listView);
        goBack = findViewById(R.id.ViewWord_goBack);
        toHome = findViewById(R.id.ViewWord_goHome);
        addWord = findViewById(R.id.ViewWord_addWord);
        topicInfo = getSharedPreferences(PREFERENCES_VIEWTOPIC,MODE_PRIVATE);
        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        final Integer topicId = topicInfo.getInt(TOPIC_ID,0);

        displayAddButton();
        generateListView(topicId);

        if(!isTeacher()) {
            onClickListView();
        }
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewWord.this,ViewTopic.class);
                startActivity(intent);
                finish();
            }
        });
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTeacher()) {
                    Intent intent = new Intent(ViewWord.this,Teacher_Home.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(ViewWord.this,Student_Home.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        addWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTeacher()) {
                    Intent intent = new Intent(ViewWord.this,TeacherAddWord.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        //delete function
        if(isTeacher()) {
//            change the icons from the childs ones to the adult ones
            toHome.setImageResource(R.drawable.homebuttonteacher);
            goBack.setImageResource(R.drawable.whitearrow);
            SwipeMenuCreator creator = new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    // create "delete" item
                    SwipeMenuItem deleteItem = new SwipeMenuItem(
                            getApplicationContext());
                    // set item background
                    deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                            0x3F, 0x25)));
                    // set item width
                    deleteItem.setWidth(220);
                    // set a icon
                    deleteItem.setIcon(R.drawable.ic_delete_);
                    // add to menu
                    menu.addMenuItem(deleteItem);
                }
            };
            listView.setMenuCreator(creator);
            listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                String url = userInfo.getString(BASIC_URL, null);

                @Override
                public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            // delete
                            Integer word_ID = words.get(position).getId();
                            Log.d(TAG, "onMenuItemClick: position: "+ position);
                            Log.d(TAG, "onMenuItemClick: topic id: "+ word_ID);
                            String URL = url + "word/deleteById?id=" + word_ID;

                            RequestQueue queue = Volley.newRequestQueue(ViewWord.this);
                            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            if (response.equals("Failed")) {
                                                Toast.makeText(ViewWord.this, "Delete words failed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(ViewWord.this, "Deleted", Toast.LENGTH_SHORT).show();
                                                String URL_decrementCount = url + "topic/wordDecrement?" + "id=" + getTopicId();
                                                String URL_deleteProgress = url + "progress/deleteByTopicIdAndWordName?" + "id=" + getTopicId() + "&" +
                                                        "name=" + words.get(position).getName();
                                                decrementWord decrementWord = new decrementWord();
                                                decrementWord.execute(URL_decrementCount);
                                                deleteProgress deleteProgress = new deleteProgress();
                                                deleteProgress.execute(URL_deleteProgress);
                                                list.remove(position);
                                                words.remove(position);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(ViewWord.this, "Request delete words failed", Toast.LENGTH_SHORT).show();

                                }
                            });
                            queue.add(stringRequest);
                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });
        }
    }

    private void displayAddButton() {
        if(isTeacher()) {
            addWord.setVisibility(View.VISIBLE);
        } else {
            addWord.setVisibility(View.GONE);
        }
    }

    private boolean isTeacher() {
        if(userInfo.getBoolean(Identity,false)) {
            return true;
        } else {
            return false;
        }
    }


    private void onClickListView() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //store the information of the word which will be used in the next activity
                SharedPreferences wordInfo;
                final String WORD_ID = "Key_WordId";
                final String WORD_NAME = "Key_WordName";
                final String WORD_SYLLABLE = "Key_WordSyllable";
                final String UPDATE_STATE = "Key_WordState";
                final String PREFERENCE_WORD = "Preference_Word";
                wordInfo = getSharedPreferences(PREFERENCE_WORD,MODE_PRIVATE);
                SharedPreferences.Editor editor = wordInfo.edit();
                Integer wordId = words.get(i).getId();
                String wordName = words.get(i).getName();
                String syllable = words.get(i).getSyllable();
                editor.putInt(WORD_ID,wordId);
                editor.putString(WORD_NAME,wordName);
                editor.putString(WORD_SYLLABLE,syllable);
                editor.apply();
                Intent intent = new Intent(ViewWord.this,Learnwords.class);
                startActivity(intent);
                finish();
            }
        });
    }

    //Getting all the words associated to the topic that is obtained from the last page
    private void generateListView(Integer topicId) {
        Integer id = topicId;
        String url = userInfo.getString(BASIC_URL,null);
        String URL = url + "word/getWordByTopicId?" + "id=" + id;
        Log.d(TAG, "generateListView: URL = " +URL);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        list = new ArrayList<>();
                        words = Word.fromJson(response);
                        for(Word word : words) {
                            list.add(word.getName().toLowerCase());
                            Log.d(TAG, "onResponse: word added = "+ word.getName().toLowerCase());
                        }
                        final String TopicWordCount = "Key_TopicWordCount";
                        final String Preference_GetWordCount = "Preference_WordCount";
                        //keep track of the wordcount in this topic
                        SharedPreferences sharedPreferences = getSharedPreferences(Preference_GetWordCount,MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt(TopicWordCount,response.length());
                        editor.commit();
                        adapter = new ArrayAdapter<String>(ViewWord.this,R.layout.customlayout,R.id.CustomTextView,list);
                        listView.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: error");
            }
        });
        RequestQueue queue = Volley.newRequestQueue(ViewWord.this);
        queue.add(jsonArrayRequest);

    }

    //after deleting the word the topic word count will decrese by 1
    private class decrementWord extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, strings[0], new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(ViewWord.this);
            requestQueue.add(stringRequest);
            return null;
        }
    }

    //after deleting the word the progresss associated to this word will all be deleted
    private class deleteProgress extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, strings[0], new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ViewWord.this, "Deleted corresponding progress failed", Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(ViewWord.this);
            requestQueue.add(stringRequest);
            return null;
        }
    }

    //get the topicId from the sharePreference
    private Integer getTopicId() {
        String TOPIC_ID = "Key_TopicId";
        String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
        Integer topicId = topicInfo.getInt(TOPIC_ID,0);
        return topicId;
    }


    private boolean isWordTableEmpty(){
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Word ",null);
        int amount=0;
        amount=cursor.getCount();
        if(amount == 0) {
            return true;
        }else {
            return false;
        }
    }

}
