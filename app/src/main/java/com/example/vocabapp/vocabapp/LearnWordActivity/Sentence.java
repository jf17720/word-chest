package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;

import java.io.IOException;
import java.util.UUID;

public class Sentence extends AppCompatActivity {
    private String BASIC_URL ;
    private MyDatabaseHelper dbHelper;
    private String currentSentence = "";
    private String theWord;
    SharedPreferences topicInfo;
    final String PREFERENCE_VIEWTOPIC = "Preference_ViewTopic";
    private TextView showWord;
    private ImageButton simpleRecord;
    private ImageButton simplePlay;
    private TextView playText;
    private TextView recordText;
    private ImageButton goback, toHome;
    private boolean ifStarted = true;
    private boolean ifPaused = true;
    String pathSave = "";
    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;
    final int REQUEST_PERMISSION_CODE = 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sentence);

        Intent intent = getIntent();
        theWord = intent.getStringExtra("the_word");
        showWord = findViewById(R.id.showWord_sentence);
        showWord.setText(theWord);
        topicInfo = getSharedPreferences(PREFERENCE_VIEWTOPIC, Context.MODE_PRIVATE);
        BASIC_URL = getURL();

        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()});

        if(cursor.moveToFirst()){
            do{
                pathSave = cursor.getString(cursor.getColumnIndex("sentence"));
            } while (cursor.moveToNext());
            cursor.close();
        }

        simplePlay = (ImageButton) findViewById(R.id.PlayButton3);
        simpleRecord = (ImageButton) findViewById(R.id.RecordButton3);
        playText = findViewById(R.id.textView17);
        recordText = findViewById(R.id.textView16);
        if(!checkPermissionFromDevice()){
            requestPermission();
        }
        ImageButton backButton = (ImageButton) findViewById(R.id.backFromSentence);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Sentence.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromSentence);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Sentence.this, Student_Home.class);
                startActivity(intent);
            }

        });
        simpleRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecord(ifStarted);
                ifStarted = !ifStarted;

            }
        });

        simplePlay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onPlay(ifPaused);
                ifPaused = !ifPaused;
            }
        });

    }

    private void onRecord(boolean start){
        if(start){
            startRecord();
            simpleRecord.setImageResource(R.drawable.stoprecordbtn);
            recordText.setText("Tap to stop");
        } else {
            stopRecord();
            simpleRecord.setImageResource(R.drawable.recordbtn);
            recordText.setText("Tap to Record");

        }
    }

    private void onPlay(boolean playback){
        if(playback){
            startPlayBack();
            simplePlay.setImageResource(R.drawable.stopplayingbtn);
            playText.setText("Tap to stop");
        } else {
            simplePlay.setImageResource(R.drawable.playbutton);
            stopPlayBack();
            playText.setText("Tap to play");
        }
    }

    private void startRecord(){
        pathSave = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/"
                + UUID.randomUUID().toString()+"_acdiorecord.3gp";
        setupMediaRecorder();

        try{
            mediaRecorder.prepare();
            mediaRecorder.start();
            final CRUD_interface word = new DatabaseController_Word( this,"WORD_STORE.db", null,1);
            Word sele = word.select(theWord,getStudentID());
            sele.setSentence(pathSave);
            word.update(sele);
            Log.d("database","updated:"+ sele.toString());
        }catch(IOException e){
            e.printStackTrace();
        }
        simplePlay.setEnabled(false);
        Toast.makeText(Sentence.this, "recording...",Toast.LENGTH_SHORT).show();
    }

    private void stopRecord(){
        mediaRecorder.stop();
        simplePlay.setEnabled(true);
        simpleRecord.setEnabled(true);
        Toast.makeText(Sentence.this, "stopped...",Toast.LENGTH_SHORT).show();
    }

    private void startPlayBack(){
        simpleRecord.setEnabled(false);

        mediaPlayer = new MediaPlayer();
        try{
            mediaPlayer.setDataSource(pathSave);
            mediaPlayer.prepare();

        } catch (IOException e){
            e.printStackTrace();
        }

        mediaPlayer.start();
        Toast.makeText(Sentence.this,"playing...",Toast.LENGTH_SHORT).show();
    }

    private void stopPlayBack(){
        simpleRecord.setEnabled(true);
        simpleRecord.setEnabled(true);

        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            setupMediaRecorder();
        }
    }
    private void setupMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(pathSave);
    }

    private  boolean checkPermissionFromDevice(){
        int wirte_external_storge_result = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio_result = ContextCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO);
        boolean ext_storage = (wirte_external_storge_result == PackageManager.PERMISSION_GRANTED);
        boolean rec_audio = (record_audio_result == PackageManager.PERMISSION_DENIED);
        return rec_audio && ext_storage;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        switch (requestCode){
            case REQUEST_PERMISSION_CODE: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT);
            }
        }
    }
    private Integer getStudentID() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }

    protected String addToOnlineDatabase(String updatedSentence) {
        Integer StudentID = getStudentID();
        String URL = BASIC_URL + "progress/sentence?wordName=" + theWord + "&" + "studentId=" + StudentID + "&" + "sentence=" + updatedSentence;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT,URL,
             new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response.equals("Failed")) {
//                        Toast.makeText(Sentence.this,"failed",Toast.LENGTH_SHORT).show();
                    } else {
//                        Toast.makeText(Sentence.this, "added successfully!", Toast.LENGTH_SHORT).show();
//                        finish();
                    }
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(Sentence.this,"Request failed",Toast.LENGTH_SHORT).show();
             }
         });
        RequestQueue queue = Volley.newRequestQueue(Sentence.this);
        queue.add(stringRequest);
        return null;
    }

    private String getURL() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL,null);
        return url;
    }
}
