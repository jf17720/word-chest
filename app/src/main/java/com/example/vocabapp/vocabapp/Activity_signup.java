package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Tools.AESUtils;

import org.json.JSONArray;

//this is for the registration of teachers only!!!
public class Activity_signup extends AppCompatActivity implements View.OnClickListener{

    Button signup;
    Button backtologin;
    EditText _name,uname,pass,confirm_pass;
    String Register_url = "http://130.61.29.169:8080/teacher/add?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signup = findViewById(R.id.Btn_register);
        backtologin = findViewById(R.id.Btn_backtologin);
        _name = findViewById(R.id.reg_name);
        uname = findViewById(R.id.reg_uname);
        pass = findViewById(R.id.reg_pass);
        confirm_pass = findViewById(R.id.reg_confirmpass);
        signup.setOnClickListener(this);
        backtologin.setOnClickListener(this);
    }


    //encrypt the password
    private String encrypted(String text) {
        String encrypted = "";
        try {
            encrypted = AESUtils.encrypt(text);
            Log.d("TEST", "encrypted:" + encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encrypted;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Btn_register:
                String name = _name.getText().toString().trim();
                String username = uname.getText().toString().trim();
                String password = pass.getText().toString().trim();
                String confirm_password = confirm_pass.getText().toString().trim();
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirm_password)) {
                    Toast.makeText(this,"Please make sure all fields are not empty",Toast.LENGTH_SHORT).show();
                } else {
                    if((!password.equals(confirm_password))) {
                        Toast.makeText(this,"The password you've entered did not match",Toast.LENGTH_SHORT).show();
                    } else if (username.length() < 6 || username.length() >  20){
                        Toast.makeText(this, "Length of the username should be within 6 to 20", Toast.LENGTH_SHORT).show();
                    } else {
                        RegisterTask registerTask = new RegisterTask();
                        registerTask.execute(name,username,password);
                    }
                }
                break;

            case R.id.Btn_backtologin:
                Intent intent1 = new Intent(this,MainActivity.class);
                startActivity(intent1);
                finish();
                break;
        }
    }

    private class RegisterTask extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {

            String encrypted_pass = encrypted(strings[2]);
            String URL = Register_url + "name=" + strings[0] + "&" + "username=" + strings[1] + "&" + "password=" + encrypted_pass;
            StringRequest stringRequest = new StringRequest(Request.Method.POST,URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("Failed")) {
                                Toast.makeText(Activity_signup.this,"The username has been used",Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Activity_signup.this, "Register successfully!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Activity_signup.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Activity_signup.this,"Request failed",Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue queue = Volley.newRequestQueue(Activity_signup.this);
            queue.add(stringRequest);
            return null;

        }
    }
}


