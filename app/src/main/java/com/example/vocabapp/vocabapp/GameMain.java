package com.example.vocabapp.vocabapp;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.vocabapp.vocabapp.Entity.Activity;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.LearnWordActivity.RecordWord;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Model.ViewTopic;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static java.lang.Thread.sleep;
//
//Game Activity
//
public class GameMain extends AppCompatActivity {
    int clueNum; //Clue number for the current word
    int wordNum; // Number of words completed
    double cluesTaken; //Total number of clues taken through the game
    //Linking the activity with appropriate rotation
    List<Activity> activities = Arrays.asList(new Activity("Photo", R.anim.rotate9),
            new Activity( "Video", R.anim.rotate4), new Activity("Initial Sound", R.anim.rotate3),
            new Activity("Word", R.anim.rotate2), new Activity("Syllables",R.anim.rotate6),
            new Activity("Rhyme",R.anim.rotate), new Activity("Category",R.anim.rotate1),
            new Activity("Look and Feel",R.anim.rotate5), new Activity( "Association",R.anim.rotate8),
            new Activity( "Sentence",R.anim.rotate7 ));
    //Declaring all the elements of the page
    ImageView photoClue;
    TextView textClue;
    TextView audioName;
    VideoView videoClue;
    ImageButton playButton;
    Button guessButton;
    Button tempButton;
    Button resetButton;
    EditText guessText;
    List<Word> words = new ArrayList<>();
    ArrayList<String> HintWords = new ArrayList<>();
    Word currentword;
    ImageView Spinner;
    ImageView selector;
    Random rand;
    Button bWord1;
    Button bWord2;
    Button bWord3;
    ImageView coin1;
    ImageView coin2;
    ImageView coin3;
    Button nextWord;
    MediaPlayer mediaPlayer;
    SharedPreferences topicInfo;
    private static final String TOPIC_NAME = "Key_TopicName";
    private static final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
    private String TAG = "GameMain";
    int n;
    final int REQUEST_PERMISSION_CODE = 1000;
    MyDatabaseHelper dbHelper;
    Integer playTrack = 0; // 1 = audioword, 2 = initialsound, 3 = sentence
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        rand = new Random();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_main);
        topicInfo = getSharedPreferences(PREFERENCES_VIEWTOPIC,MODE_PRIVATE);
        String topicName = topicInfo.getString(TOPIC_NAME,null);
        textClue = findViewById(R.id.textClue);
        photoClue = findViewById(R.id.photoClue);
        videoClue = findViewById(R.id.videoClue);
        playButton = findViewById(R.id.playbutton);
        guessText = findViewById(R.id.guessText);
        resetButton = findViewById(R.id.reset);
        tempButton = findViewById(R.id.nextClue);
        guessButton = findViewById(R.id.guessClue);
        audioName = findViewById(R.id.AudioName);
        Spinner = findViewById(R.id.spinner);
        selector = findViewById(R.id.selector);
        bWord1 = findViewById(R.id.button1);
        bWord2 = findViewById(R.id.button2);
        bWord3 = findViewById(R.id.button3);
        coin1 = findViewById(R.id.coin1);
        coin2 = findViewById(R.id.coin2);
        coin3 = findViewById(R.id.coin3);
        nextWord = findViewById(R.id.nextWord);
        audioName = findViewById(R.id.AudioName);

        int count = 0;
        n=0;
        ClearFullDisplay();
        requestPermission();

        Toast.makeText(GameMain.this,"Spin to get clue and guess the word",Toast.LENGTH_LONG).show();



        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from Word where topic=? and std_id=?", new String[]{topicName,getStudentID().toString()});

        while (cursor.moveToNext()){
            String cword  = cursor.getString(cursor.getColumnIndex("word"));
            String rhyme  = cursor.getString(cursor.getColumnIndex("rhyme"));
            String association  = cursor.getString(cursor.getColumnIndex("association"));
            String sentence  = cursor.getString(cursor.getColumnIndex("sentence"));
            String category  = cursor.getString(cursor.getColumnIndex("category"));
            String lookAndFeel  = cursor.getString(cursor.getColumnIndex("lookAndFeel"));
            String photoPath  = cursor.getString(cursor.getColumnIndex("photoPath"));
            String audioPath  = cursor.getString(cursor.getColumnIndex("audioPath"));
            String wordAudioPath = cursor.getString(cursor.getColumnIndex("wordAudioPath"));
            String videoPath = cursor.getString(cursor.getColumnIndex("videoPath"));
            Integer syllablefinished = cursor.getInt(cursor.getColumnIndex("syllableFinished"));
            HintWords.add(cword);
            if(!rhyme.equals("") && !association.equals("") && !sentence.equals("") && syllablefinished!=0 && !videoPath.equals("")
                    && !category.equals("") && !lookAndFeel.equals("") && !photoPath.equals("") && !audioPath.equals("") && !wordAudioPath.equals("")){
                Word word = new Word();
                word.setId(count);
                word.setName(cword);
                word.setRhyme(rhyme);
                word.setAssociation(association);
                word.setSentence(sentence);
                word.setCategory(category);
                word.setLookAndFeel(lookAndFeel);
                word.setPhotoPath(photoPath);
                word.setAudioPath(audioPath);
                word.setWordAudioPath(wordAudioPath);
                word.setSyllableFinished(syllablefinished);
                word.setVideoPath(videoPath);
                if (word.getRhyme().length() != 0) words.add(word);
                count++;

            }

            Log.d("testinsert",words.toString());


        }
        cursor.close();

        resetGame(); //Start the game
        ImageButton backButton = (ImageButton) findViewById(R.id.backFromMenu);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent_learn = new Intent(GameMain.this, ViewTopic.class);
                intent_learn.putExtra("game_page_start",1);
                startActivity(intent_learn);
                finish();
            }
        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromMenu);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent_learn = new Intent(GameMain.this, Student_Home.class);
                startActivity(intent_learn);
                finish();
            }
        });

        //Next hint button shows next hint unless at the end of hints
        tempButton = (Button) findViewById(R.id.nextClue);
        tempButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(clueNum<activities.size() && wordNum<words.size()) {
                    //Spin the wheel for the next clue
                    spin();
                } else{
                    //If all clues seen then display multiple choice buttons
                    multipleChoiceDisplay();
                }
            }

        });

        //Next hint button shows next hint unless at the end of hints
        resetButton = (Button) findViewById(R.id.reset);
        resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Reset the display
                ClearFullDisplay();
                DisplayGame();
                //Reset the game variables
                resetGame();
            }

        });


        guessButton = (Button) findViewById(R.id.guessClue);
        guessButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (currentword.getName().toLowerCase().equals(guessText.getText().toString().toLowerCase()) && wordNum < words.size() - 1) {
                    //Correct guess resets textbox and increments correct guesses counter
                    guessText.setText("");
                    Toast.makeText(GameMain.this, "Correct!!!", Toast.LENGTH_SHORT).show();
                    //Reset display to say correct guess
                    ClearFullDisplay();
                    inbetweenWordsDisplay();
                } else if (currentword.getName().toLowerCase().equals(guessText.getText().toString().toLowerCase()) && wordNum == words.size() - 1) {
                    //If correct and last word then display score
                    ClearFullDisplay();
                    nextWord();
                } else { //Incorrect Guess and still able to guess
                    Toast.makeText(GameMain.this, "Wrong please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Button on inbetween screens to click next word
        nextWord = (Button) findViewById(R.id.nextWord);
        nextWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Change the display
                ClearFullDisplay();
                DisplayGame();
                //Get next word to guess
                nextWord();
            }
        });


        //
        //Buttons for the multiple choice display if button text is equal to the word being guessed
        // then change display and get next word if not then button disappears
        //
        bWord1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                if(bWord1.getText()==currentword.getName()){
                    Toast.makeText(GameMain.this,"Correct!!!",Toast.LENGTH_SHORT).show();
                    DisplayGame();
                    nextWord();
                } else{
                    bWord1.setVisibility(View.INVISIBLE);
                    clueNum++;
                }
            }
        });

        bWord2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                if(bWord2.getText()==currentword.getName()){
                    Toast.makeText(GameMain.this,"Correct!!!",Toast.LENGTH_SHORT).show();
                    DisplayGame();
                    nextWord();
                } else{
                    bWord2.setVisibility(View.INVISIBLE);
                    Toast.makeText(GameMain.this,"Incorrect",Toast.LENGTH_SHORT).show();
                    clueNum++;
                }
            }
        });

        bWord3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                if(bWord3.getText()==currentword.getName()){
                    Toast.makeText(GameMain.this,"Correct!!!",Toast.LENGTH_SHORT).show();
                    DisplayGame();
                    nextWord();
                } else{
                    bWord3.setVisibility(View.INVISIBLE);
                    Toast.makeText(GameMain.this,"Incorrect",Toast.LENGTH_SHORT).show();
                    clueNum++;
                }
            }
        });

        //Replay audio when button clicked
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPlayBack();
            }
        });

    }

    //Reset the game including display and variables
    private void resetGame(){
        ClearFullDisplay();
        DisplayGame();
        wordNum=0;
        cluesTaken=0;
        //Shuffle activities and words so the order is random
        Collections.shuffle(activities);
        Collections.shuffle(words);
        currentword=words.get(wordNum);
        clueNum=0;
    }

    private void nextWord(){
        //If at the end of the list of words display score
        if(wordNum==words.size()-1){
            wordNum++;
            ClearFullDisplay();
            DisplayScore();
            cluesTaken=cluesTaken+clueNum;
            //Calculate percentage score
            double averageCluesTaken = cluesTaken/wordNum;
            double inverseScore = (13 - averageCluesTaken)/12;
            double percentageScore = inverseScore*100;
            long score = Math.round(percentageScore);
            //Display number of coins based on percentage score
            //100% means the user took 1 clue for each word guessed
            if(score<50){
                displayCoins(1);
            } else if(score<75){
                displayCoins(2);
            } else{
                displayCoins(3);
            }
            textClue.setText("End of topic");
        } else if(wordNum<words.size()-1) {
            //If not at the end of list of words get next word
            ClearButtonDisplay();
            wordNum++;
            cluesTaken=cluesTaken+clueNum;
            clueNum=0;
            currentword = words.get(wordNum);
            //Randomise order of activities
            Collections.shuffle(activities);
        }
    }

    //Given an activity display the clue
    private void displayClue(String Clue, Word word) {
        //Reset the display
        ClearClueDisplay();
        Spinner.setVisibility(View.INVISIBLE);
        selector.setVisibility(View.INVISIBLE);
        switch (Clue) {
            case "Photo":
                photoClue.setVisibility(View.VISIBLE);
                loadPhotoFromGallery();
                break;
            case "Video":
                videoClue.setVisibility(View.VISIBLE);
                loadVideoFromGallery();
                break;
            case "Initial Sound":
                playButton.setVisibility(View.VISIBLE);
                audioName.setVisibility(View.VISIBLE);
                audioName.setText("Initial Sound");
                playTrack = 2;
                break;
            case "Word":
                playButton.setVisibility(View.VISIBLE);
                audioName.setVisibility(View.VISIBLE);
                audioName.setText("Word Sound");
                playTrack = 1;
                break;
            case "Sentence":
                playButton.setVisibility(View.VISIBLE);
                audioName.setVisibility(View.VISIBLE);
                audioName.setText("Sentence Sound");
                playTrack = 3;
                break;
            case "Syllables":
                textClue.setText(Clue+ ": \n"+currentword.getSyllableFinished());
                textClue.setVisibility(View.VISIBLE);
                break;
            case "Rhyme":
                textClue.setText(Clue+ ": \n"+currentword.getRhyme());
                textClue.setVisibility(View.VISIBLE);
                break;
            case "Association":
                textClue.setText(Clue+ ": \n"+currentword.getAssociation());
                textClue.setVisibility(View.VISIBLE);
                break;
            case "Category":
                textClue.setText(Clue+ ": \n"+currentword.getCategory());
                textClue.setVisibility(View.VISIBLE);
                break;
            case "Look and Feel":
                textClue.setText(Clue+ ": \n"+currentword.getLookAndFeel());
                textClue.setVisibility(View.VISIBLE);
                break;
        }
    }

    //Load the photo from the file system and display it
    private void loadPhotoFromGallery(){
        Matrix matrix = new Matrix();
//        matrix.postRotate(90);
        Bitmap myBitmap = BitmapFactory.decodeFile(currentword.getPhotoPath());
        if(myBitmap!=null) {
            Bitmap rotatedBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
            photoClue.setImageBitmap(rotatedBitmap);
        }
    }

    //Load the video from the file system and display it
    private void loadVideoFromGallery(){
        String mCurrentVideoPath = currentword.getVideoPath();
        System.out.print(mCurrentVideoPath);
        String uriPath = mCurrentVideoPath;
        Log.d("Video", mCurrentVideoPath);
        if(!mCurrentVideoPath.matches("")){
            File videoFile = new File(uriPath);
            Uri video = FileProvider.getUriForFile(this, "com.example.vocabapp.vocabapp.fileprovider", videoFile);
            Log.d("Video", video.toString());
            try{
                videoClue.setVideoURI(video);
                videoClue.start();
            } catch (Exception e){
                Log.d(TAG, "loadVideo: failed");
            }
        }
    }

    //Display multiple choice buttons
    private void multipleChoiceDisplay(){
        //Clear display
        ClearClueDisplay();
        if(HintWords.size() == 1){
            oneButtonDisplay(HintWords.get(0));
        } else if(HintWords.size() == 2) {
            Collections.shuffle(HintWords);
            twoButtonDisplay(HintWords.get(0),HintWords.get(1));
        } else {
            ArrayList<String> wordToDisplay = new ArrayList<>();
            wordToDisplay.add(currentword.getName());
            Random rand = new Random();

            while (wordToDisplay.size() < 3){
                String randomPick = HintWords.get(rand.nextInt(HintWords.size()));
                if (!wordToDisplay.contains(randomPick)){
                    wordToDisplay.add(randomPick);
                }
            }
            Collections.shuffle(wordToDisplay);
            threeButtonDisplay(wordToDisplay.get(0),wordToDisplay.get(1),wordToDisplay.get(2));
        }
    }

    //
    //Functions to change displays
    //
    private void oneButtonDisplay(String word1){
        bWord1.setText(word1);
        bWord1.setVisibility(View.VISIBLE);
        guessText.setVisibility(View.INVISIBLE);
        guessButton.setVisibility(View.INVISIBLE);
        tempButton.setVisibility(View.INVISIBLE);
    }

    private void inbetweenWordsDisplay(){
        textClue.setVisibility(View.VISIBLE);
        textClue.setText("Correct guess");
        nextWord.setVisibility(View.VISIBLE);
    }

    private void threeButtonDisplay(String word1, String word2, String word3){
        bWord1.setText(word1);
        bWord2.setText(word2);
        bWord3.setText(word3);
        bWord1.setVisibility(View.VISIBLE);
        bWord2.setVisibility(View.VISIBLE);
        bWord3.setVisibility(View.VISIBLE);
        guessText.setVisibility(View.INVISIBLE);
        guessButton.setVisibility(View.INVISIBLE);
        tempButton.setVisibility(View.INVISIBLE);
    }
    private void twoButtonDisplay(String word1, String word2){
        bWord1.setText(word1);
        bWord2.setText(word2);
        bWord1.setVisibility(View.VISIBLE);
        bWord2.setVisibility(View.VISIBLE);
        guessText.setVisibility(View.INVISIBLE);
        guessButton.setVisibility(View.INVISIBLE);
        tempButton.setVisibility(View.INVISIBLE);
    }

    private void ClearButtonDisplay(){
        bWord1.setVisibility(View.INVISIBLE);
        bWord2.setVisibility(View.INVISIBLE);
        bWord3.setVisibility(View.INVISIBLE);
    }

    private void ClearClueDisplay(){
        photoClue.setVisibility(View.INVISIBLE);
        videoClue.setVisibility(View.INVISIBLE);
        textClue.setVisibility(View.INVISIBLE);
        playButton.setVisibility(View.INVISIBLE);
        audioName.setVisibility(View.INVISIBLE);
    }

    private void ClearFullDisplay(){
        photoClue.setVisibility(View.INVISIBLE);
        videoClue.setVisibility(View.INVISIBLE);
        textClue.setVisibility(View.INVISIBLE);
        playButton.setVisibility(View.INVISIBLE);
        guessText.setVisibility(View.INVISIBLE);
        guessButton.setVisibility(View.INVISIBLE);
        tempButton.setVisibility(View.INVISIBLE);
        resetButton.setVisibility(View.INVISIBLE);
        Spinner.setVisibility(View.INVISIBLE);
        selector.setVisibility(View.INVISIBLE);
        bWord1.setVisibility(View.INVISIBLE);
        bWord2.setVisibility(View.INVISIBLE);
        bWord3.setVisibility(View.INVISIBLE);
        coin1.setVisibility(View.INVISIBLE);
        coin2.setVisibility(View.INVISIBLE);
        coin3.setVisibility(View.INVISIBLE);
        nextWord.setVisibility(View.INVISIBLE);
        audioName.setVisibility(View.INVISIBLE);
    }

    private void DisplayGame(){
        Spinner.setVisibility(View.VISIBLE);
        selector.setVisibility(View.VISIBLE);
        guessText.setVisibility(View.VISIBLE);
        guessButton.setVisibility(View.VISIBLE);
        tempButton.setVisibility(View.VISIBLE);
    }

    private void DisplayScore(){
        resetButton.setVisibility(View.VISIBLE);
        textClue.setVisibility(View.VISIBLE);
        Spinner.setVisibility(View.INVISIBLE);
    }

    private void displayCoins(int num){
        switch(num){
            case 1:
                coin2.setImageResource(R.drawable.coingrey);
                coin3.setImageResource(R.drawable.coingrey);
                break;
            case 2:
                coin2.setImageResource(R.drawable.coin);
                coin3.setImageResource(R.drawable.coingrey);
                break;
            case 3:
                coin2.setImageResource(R.drawable.coin);
                coin3.setImageResource(R.drawable.coin);
        }
        coin1.setVisibility(View.VISIBLE);
        coin2.setVisibility(View.VISIBLE);
        coin3.setVisibility(View.VISIBLE);
    }

    //Spin the spinner and call display for the next clue
    private void spin(){
        Spinner.setVisibility(View.VISIBLE);
        audioName.setVisibility(View.INVISIBLE);
        selector.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(GameMain.this, activities.get(clueNum).actAnim);
        animation.setFillAfter(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //At end of the animation call display for the clue
                displayClue(activities.get(clueNum).actName, currentword);
                clueNum++;
                Toast.makeText(GameMain.this,"Spin again!",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Spinner.setVisibility(View.INVISIBLE);
            }
        });
        Spinner.startAnimation(animation);


    }
    //Get the permission requests
    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }

    private void startPlayBack(){

        mediaPlayer = new MediaPlayer();
        try{
            if(playTrack == 1){
                mediaPlayer.setDataSource(currentword.getWordAudioPath());
            }
            if(playTrack == 2){
                mediaPlayer.setDataSource(currentword.getAudioPath());
            }
            if(playTrack == 3){
                mediaPlayer.setDataSource(currentword.getSentence());
            }
            mediaPlayer.prepare();

        } catch (IOException e){
            e.printStackTrace();
        }

        mediaPlayer.start();
        Toast.makeText(GameMain.this,"playing...",Toast.LENGTH_SHORT).show();
    }

    private Integer getStudentID() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }
}
