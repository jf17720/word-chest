package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Model.ViewStudent;
import com.example.vocabapp.vocabapp.Tools.AESUtils;

//this is for teacher to register students
public class StudentRegister extends AppCompatActivity implements View.OnClickListener {

    Button signup,backToMenu;
    ImageButton goBack;
    EditText _name,uname,pass,confirm_pass;
    Integer teacherId;
    String Register_url = "http://130.61.29.169:8080/student/add?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_register);

        signup = findViewById(R.id.Btn_registerStu);
        backToMenu = findViewById(R.id.Btn_backToMenu);
        goBack = findViewById(R.id.StudentReg_goBack);
        _name = findViewById(R.id.regStu_name);
        uname = findViewById(R.id.regStu_uname);
        pass = findViewById(R.id.regStu_pass);
        confirm_pass = findViewById(R.id.regStu_confirmpass);
        teacherId = getTeacherId();
        signup.setOnClickListener(this);
        backToMenu.setOnClickListener(this);
        goBack.setOnClickListener(this);

    }




    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Btn_registerStu:
                String name = _name.getText().toString().trim();
                String username = uname.getText().toString().trim();
                String password = encrypted(pass.getText().toString().trim());
                String confirm_password = encrypted(confirm_pass.getText().toString().trim());
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirm_password)) {
                    Toast.makeText(this,"Please make sure all fields are not empty",Toast.LENGTH_SHORT).show();
                } else if((!password.equals(confirm_password))) {
                    Toast.makeText(this, "The password you've entered did not match", Toast.LENGTH_SHORT).show();
                } else if(username.length() < 6 || username.length() > 20) {
                    Toast.makeText(this, "Length of the username should be within 6 to 20", Toast.LENGTH_SHORT).show();
                } else {
                    RegisterTask registerTask = new RegisterTask();
                    registerTask.execute(name,username,password);
                }
                break;


            case R.id.Btn_backToMenu:
                Intent intent1 = new Intent(this,Teacher_Home.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.StudentReg_goBack:
                Intent intent2 = new Intent(this,ViewStudent.class);
                startActivity(intent2);
                finish();
                break;
        }
    }

    //encrypt the password
    private String encrypted(String text) {
        String encrypted = "";
        try {
            encrypted = AESUtils.encrypt(text);
            Log.d("TEST", "encrypted:" + encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encrypted;
    }

    private class RegisterTask extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {

            String url = Register_url + "name=" + strings[0] + "&" + "username=" + strings[1] + "&" + "password=" + strings[2]
                   + "&" + "teacherId=" + teacherId;
            StringRequest stringRequest = new StringRequest(Request.Method.GET,url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("Failed")) {
                                Toast.makeText(StudentRegister.this,"The username has been used",Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(StudentRegister.this,    "Register successfully!", Toast.LENGTH_SHORT).show();
                                Intent intent_homepage = new Intent(StudentRegister.this, ViewStudent.class);
                                startActivity(intent_homepage);
                                finish();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(StudentRegister.this,"Request failed",Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue queue = Volley.newRequestQueue(StudentRegister.this);
            queue.add(stringRequest);
            return null;


        }
    }
    private Integer getTeacherId() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Teacher_ID = "Key_TeacherId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer id = sharedPreferences.getInt(Teacher_ID,0);
        return id;
    }
}
