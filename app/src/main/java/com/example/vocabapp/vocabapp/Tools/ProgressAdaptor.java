package com.example.vocabapp.vocabapp.Tools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vocabapp.vocabapp.Entity.Student;
import com.example.vocabapp.vocabapp.R;


import java.util.ArrayList;

public class ProgressAdaptor extends BaseAdapter {

    private ArrayList<Student> mData;
    private LayoutInflater mInflater;
    private Integer wordCount;

    public ProgressAdaptor(LayoutInflater inflater, ArrayList<Student> data, Integer wordCount){

        this.mInflater = inflater;
        this.mData = data;
        this.wordCount = wordCount;

    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        //get the view
        View progressView = mInflater.inflate(R.layout.custom_layout2,null);
        Student student = mData.get(position);

        //Getting each item from the layout
        TextView textView_Main = progressView.findViewById(R.id.Custom2TextView_Main);
        TextView textView_Progress = progressView.findViewById(R.id.Custom2_Progress);

        textView_Main.setText(student.getName().trim());//show the student name
        String show = student.getWordCompleted().toString() + "/" + wordCount; //e.g: 2/10
        textView_Progress.setText(show);
        textView_Progress.setVisibility(View.VISIBLE);

        return progressView;

    }
}
