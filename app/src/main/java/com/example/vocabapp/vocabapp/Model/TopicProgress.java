package com.example.vocabapp.vocabapp.Model;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Progress;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Teacher_Home;
import com.example.vocabapp.vocabapp.Tools.ProgressAdaptor;
import com.example.vocabapp.vocabapp.Tools.TopicProgressAdaptor;

import org.json.JSONArray;

import java.util.ArrayList;

public class TopicProgress extends AppCompatActivity {

    SharedPreferences userInfo;
    private static final String Teacher_ID = "Key_TeacherId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    private ImageButton goBack;
    private ListView listView;
    private ImageView toHome;
    private TextView display;
    Integer teacherId;
    private ArrayList<Topic> topics;
    private ArrayList<Progress> progresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_progress);

        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        listView = findViewById(R.id.TopicProgress_listView);
        goBack = findViewById(R.id.TopicProgress_goBack);
        toHome = findViewById(R.id.TopicProgress_goHome);
        display = findViewById(R.id.TopicProgress_display);
        teacherId = userInfo.getInt(Teacher_ID,0);

        Display();
//        getProgressByStudentId();
        generateView generateView = new generateView();
        generateView.execute(teacherId.toString());
        onClickListView();
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(TopicProgress.this,StudentProgress.class);
               startActivity(intent);
            }
        });
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TopicProgress.this,Teacher_Home.class);
                startActivity(intent);
            }
        });

    }

    private Integer getStudentId() {
        final String STUDENT_ID = "Key_StudentID";
        final String STUDENT_NAME = "Key_StudentName";
        final String PREFERENCES_STUDENT = "Preference_Student";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_STUDENT,MODE_PRIVATE);
        Integer id = sharedPreferences.getInt(STUDENT_ID,0);
        return id;
    }

    //display the student name on the top middle of the screen
    private void Display() {
        final String STUDENT_ID = "Key_StudentID";
        final String STUDENT_NAME = "Key_StudentName";
        final String PREFERENCES_STUDENT = "Preference_Student";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_STUDENT,MODE_PRIVATE);
        String name = sharedPreferences.getString(STUDENT_NAME,null);
        display.setText(name);
    }


    private class generateView extends  AsyncTask<String,String,String> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(TopicProgress.this,"Loading","Please wait...",false,false);
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            //Dismissing the progress dialog
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(String... strings) {
            Integer id = Integer.valueOf(strings[0]);
            String url = userInfo.getString(BASIC_URL,null);
            String URL = url + "topic/getTopicByTeacherId?" + "id=" + id;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            topics = Topic.fromJson(response);
                            LayoutInflater inflater = getLayoutInflater();
                            //the adaptor will list all the topics in a listview and display the progress of each topic
                            TopicProgressAdaptor topicProgressAdaptor = new TopicProgressAdaptor(inflater,topics);
                            listView.setAdapter(topicProgressAdaptor);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            RequestQueue queue = Volley.newRequestQueue(TopicProgress.this);
            queue.add(jsonArrayRequest);
            return null;
        }
    }


//    private void getProgressByStudentId() {
//        String url = userInfo.getString(BASIC_URL,null);
//        String URL = url + "/progress/getProgressByStudent?id=" + getStudentID();
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        progresses = Progress.fromJson(response);
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                Toast.makeText(TopicProgress.this, "Request failed", Toast.LENGTH_SHORT).show();
//            }
//        });
//        RequestQueue rQueue = Volley.newRequestQueue(TopicProgress.this);
//        rQueue.add(jsonArrayRequest);
//    }

    private Integer getStudentID() {
        final String STUDENT_ID = "Key_StudentID";
        final String PREFERENCES_TopicProgress = "Preference_ViewTopic";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_TopicProgress,MODE_PRIVATE);
        return sharedPreferences.getInt(STUDENT_ID,0);
    }


    private void onClickListView() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences;
                final String TOPIC_ID = "Key_TopicId";
                final String TOPIC_NAME = "Key_TopicName";
                final String STUDENT_ID = "Key_StudentID";
                final String PREFERENCES_TopicProgress = "Preference_ViewTopic";
                sharedPreferences = getSharedPreferences(PREFERENCES_TopicProgress,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Integer topic_ID = topics.get(i).getId();
                String topic_Name = topics.get(i).getName();
                editor.putInt(TOPIC_ID,topic_ID);
                editor.putInt(STUDENT_ID,getStudentId());
                editor.putString(TOPIC_NAME,topic_Name);
                editor.apply();
                Intent intent = new Intent(TopicProgress.this,WordProgress.class);
                startActivity(intent);
            }
        });
    }





}

