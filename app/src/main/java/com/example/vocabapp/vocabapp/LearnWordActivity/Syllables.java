package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;

import static com.example.vocabapp.vocabapp.TeacherMenu.PREFERENCES_StoreId;

public class Syllables extends AppCompatActivity {

    public static int SyllablesVisible = 0;
    private Integer SyllableCount = 0;
    private MyDatabaseHelper dbHelper;
    private String theWord;
    private ImageView[] Images;
    private TextView text;
    private Integer currentSyllableCount;
    private TextView syllableText1;
    private TextView syllableText2;
    private TextView syllableText3;
    private TextView syllableText4;
    private TextView syllableText5;
    private TextView syllableText6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllables);

        Intent intent = getIntent();
        theWord = intent.getStringExtra("the_word");

        text = findViewById(R.id.syllablesQuestion);
        String question = String.format("How many syllables does %s have?", theWord);
        text.setText(question);


        syllableText1 = findViewById(R.id.syllableText1);
        syllableText2 = findViewById(R.id.syllableText2);
        syllableText3 = findViewById(R.id.syllableText3);
        syllableText4 = findViewById(R.id.syllableText4);
        syllableText5 = findViewById(R.id.syllableText5);
        syllableText6 = findViewById(R.id.syllableText6);


        ImageButton backButton = (ImageButton) findViewById(R.id.backSyllables);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Syllables.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromSyllables);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Syllables.this, Student_Home.class);
                startActivity(intent);
            }

        });

        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null, 1);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord, getStudentID().toString()});
        if (cursor.moveToFirst()) {
            do {
                currentSyllableCount = cursor.getInt(cursor.getColumnIndex("syllableFinished"));
                SyllableCount = currentSyllableCount;

            } while (cursor.moveToNext());
            cursor.close();
        }

        final String WORD_ID = "Key_WordId";
        final String WORD_NAME = "Key_WordName";
        final String WORD_SYLLABLE = "Key_WordSyllable";
        final String PREFERENCE_WORD = "Preference_Word";
        SharedPreferences wordInfo = getSharedPreferences(PREFERENCE_WORD, MODE_PRIVATE);
        String NumOfSyllable = wordInfo.getString(WORD_SYLLABLE, null);
        SyllableCount = Integer.parseInt(NumOfSyllable);
        System.out.println(SyllableCount);


        final ImageButton syllable1 = (ImageButton) findViewById(R.id.syllables1); //when user clicks on one syllable
        syllable1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("1");
                if (SyllableCount == 1) { //if correct
                    correctAnswer();
                } else { //if incorrect
                    incorrectAnswer(syllable1, syllableText1);
                }
            }

        });

        final ImageButton syllable2 = (ImageButton) findViewById(R.id.syllables2); //when user clicks on two syllables
        syllable2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SyllableCount == 2) { //if correct
                    correctAnswer();
                } else { //if incorrect
                    incorrectAnswer(syllable2, syllableText2);
                }
            }

        });

        final ImageButton syllable3 = (ImageButton) findViewById(R.id.syllables3); //when user clicks on 3 syllables
        syllable3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SyllableCount == 3) { //if correct
                    correctAnswer();
                } else {
                    incorrectAnswer(syllable3, syllableText3);
                }
            }

        });

        final ImageButton syllable4 = (ImageButton) findViewById(R.id.syllables4); //when user clicks on 4 syllables
        syllable4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SyllableCount == 4) { //if correct
                    correctAnswer();
                } else { //if incorrect
                    incorrectAnswer(syllable4, syllableText4);
                }
            }

        });

        final ImageButton syllable5 = (ImageButton) findViewById(R.id.syllables5); //when user clicks on 5 syllables
        syllable5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SyllableCount == 5) { //if correct
                    correctAnswer();
                } else { //if incorrect
                    incorrectAnswer(syllable5, syllableText5);
                }
            }

        });

        final ImageButton syllable6 = (ImageButton) findViewById(R.id.syllables6); //when user clicks on 6 syllables
        syllable6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SyllableCount == 6) { //if correct
                    correctAnswer();
                } else { //if incorrect
                    incorrectAnswer(syllable6, syllableText6);
                }
            }

        });


    }

    private String getURL() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL,null);
        return url;
    }

    protected String addToOnlineDatabase(Integer syllableCount) {
        String syllable = String.valueOf(syllableCount);
        Integer StudentID = getStudentID();
        String URL = getURL() + "progress/syllable?wordName=" + theWord + "&" + "studentId=" + StudentID + "&" + "syllable=" + syllable;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT,URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("Failed")) {
//                        Toast.makeText(Sentence.this,"failed",Toast.LENGTH_SHORT).show();
                        } else {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Syllables.this,"Request failed",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(Syllables.this);
        queue.add(stringRequest);
        return null;
    }

//add the amount of syllables the child has entered into the database
    private void storeSyllable (Integer SyllableCount){
        final CRUD_interface database = new DatabaseController_Word(this, "WORD_STORE.db", null, 1);
        Word w = database.select(theWord, getStudentID());
        w.setSyllableFinished(SyllableCount);
        database.update(w);
        Log.d("database", "updated:" + w.toString());
        Learnwords.green++;
    }

// find the unique identifier for the student accessing this page
    private Integer getStudentID() {
        final String Student_ID = "Key_StudentId";
        final String PREFERENCES_StoreId = "Preference_Id";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID, 0);

        return ID;

    }

// if the amount of syllables is correct, store it in the database, give them a message telling them its correct and send them back to the learn words page
    public void correctAnswer() {
        storeSyllable(SyllableCount);
        addToOnlineDatabase(SyllableCount);
        Toast.makeText(Syllables.this, "Correct answer", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Syllables.this, Learnwords.class);
        startActivity(intent);
    }

//    if they clicked on the wrong button then remove it and tell them to try again
    public void incorrectAnswer (ImageButton button, TextView txt){
        Toast.makeText(Syllables.this, "Please try again", Toast.LENGTH_SHORT).show();
        button.setVisibility(View.GONE);
        txt.setVisibility(View.GONE);
    }


}