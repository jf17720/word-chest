package com.example.vocabapp.vocabapp.Entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Teacher {

    private Integer id;

    private String name;

    private String username;

    private String password;

    private ArrayList<Student> students;

    private ArrayList<Topic> topics;

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public ArrayList<Student> getStudents() {

        return students;
    }

    public void setStudents(ArrayList<Student> students) {

        this.students = students;
    }

    public void setTopics(ArrayList<Topic> topics) {

        this.topics = topics;
    }

    public ArrayList<Topic> getTopics() {

        return topics;

    }

    //Convert from json object to java object
    public static Teacher fromJson(JSONObject jsonObject) {
        Teacher teacher = new Teacher();

        try {
            teacher.id = jsonObject.getInt("id");
            teacher.name = jsonObject.getString("name");
            teacher.username = jsonObject.getString("username");
            teacher.password = jsonObject.getString("password");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return teacher;
    }

    //Convert from json array to java array of objects
    public static ArrayList<Teacher> fromJson(JSONArray jsonArray) {
        JSONObject userJson;
        ArrayList<Teacher> teachers = new ArrayList<Teacher>(jsonArray.length());

        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                userJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Teacher teacher = Teacher.fromJson(userJson);
            if(teacher != null) {
                teachers.add(teacher);
            }
        }
        return teachers;

    }
}
