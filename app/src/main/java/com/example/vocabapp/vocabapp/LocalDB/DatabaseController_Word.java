package com.example.vocabapp.vocabapp.LocalDB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.vocabapp.vocabapp.Entity.Word;

import java.security.AccessControlContext;

public class DatabaseController_Word implements CRUD_interface {
    MyDatabaseHelper helper = null;

    public DatabaseController_Word(Context context,  String name,  SQLiteDatabase.CursorFactory factory, int version){
        helper = new MyDatabaseHelper(context,name,factory,version);
    }

    @Override
    public boolean insert(Word word) { //implementation of insert operation
        boolean flag = false;
        SQLiteDatabase database = null;
        try {
            String sql = "INSERT INTO Word(word,topic,rhyme,association,sentence,category,lookAndFeel,photoPath,audioPath,videoPath,wordAudioPath,syllableFinished,std_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            database = helper.getWritableDatabase();
            database.execSQL(sql, new Object[]{word.getName(),word.getTopic(),word.getRhyme(),word.getAssociation(),word.getSentence(),
                                    word.getCategory(),word.getLookAndFeel(),word.getPhotoPath(),word.getAudioPath(),word.getVideoPath(),
                                    word.getWordAudioPath(),word.getSyllableFinished(),word.getStd_id()}); // put values into sql command
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(database!=null){
                database.close();
            }
        }
        return flag;
    }

    @Override
    public boolean delete(int id) { // implementation of delete operation
        boolean flag = false;
        SQLiteDatabase database = null;
        try {
            String sql = "DELETE FROM Word WHERE id=?";
            database = helper.getWritableDatabase();
            database.execSQL(sql, new Object[]{Integer.toString(id)});
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(database!=null){
                database.close();
            }
        }
        return flag;

    }

    @Override
    public boolean update(Word word) { // update the word row with student input
        boolean flag = false;
        SQLiteDatabase database = null;
        try {
            String sql = "UPDATE Word set std_id=?, wordAudioPath=?, syllableFinished=?, word=?, rhyme=?, topic=?, association=?, sentence=?, category=?, lookAndFeel=?, photoPath=?, audioPath=?, videoPath=? where id=?";
            database = helper.getWritableDatabase();
            database.execSQL(sql, new Object[]{word.getStd_id(),word.getWordAudioPath(),word.getSyllableFinished(),word.getName(),word.getRhyme(),word.getTopic(),word.getAssociation(),word.getSentence(),
                    word.getCategory(),word.getLookAndFeel(),word.getPhotoPath(),word.getAudioPath(),word.getVideoPath(),word.getId()});
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(database!=null){
                database.close();
            }
        }
        return flag;

    }

    @Override
    public Word select(String theword,Integer std_id) {   // select word from database using student Id and word name as identifier
        Word word = new Word();
        SQLiteDatabase database = null;
        try {
            String sql = "SELECT * FROM Word where word=? and std_id=?";
            database = helper.getReadableDatabase();
            Cursor cursor = database.rawQuery(sql, new String[]{theword,std_id.toString()});
            while(cursor.moveToNext()){
                Integer _id = cursor.getInt(cursor.getColumnIndex("id"));
                Integer _std_id = cursor.getInt(cursor.getColumnIndex("std_id"));
                String _word = cursor.getString(cursor.getColumnIndex("word"));
                String _topic = cursor.getString(cursor.getColumnIndex("topic"));
                String _rhyme = cursor.getString(cursor.getColumnIndex("rhyme"));
                String _association = cursor.getString(cursor.getColumnIndex("association"));
                String _category = cursor.getString(cursor.getColumnIndex("category"));
                String _sentence = cursor.getString(cursor.getColumnIndex("sentence"));
                String _lookAndFeel = cursor.getString(cursor.getColumnIndex("lookAndFeel"));
                String _photoPath = cursor.getString(cursor.getColumnIndex("photoPath"));
                String _audioPath = cursor.getString(cursor.getColumnIndex("audioPath"));
                String _videoPath = cursor.getString(cursor.getColumnIndex("videoPath"));
                String _wordAudioPath = cursor.getString(cursor.getColumnIndex("wordAudioPath"));
                Integer _syllableFinished = cursor.getInt(cursor.getColumnIndex("syllableFinished"));

                word.setId(_id);
                word.setStd_id(_std_id);
                word.setRhyme(_rhyme);
                word.setName(_word);
                word.setSentence(_sentence);
                word.setAssociation(_association);
                word.setCategory(_category);
                word.setAudioPath(_audioPath);
                word.setTopic(_topic);
                word.setPhotoPath(_photoPath);
                word.setLookAndFeel(_lookAndFeel);
                word.setVideoPath(_videoPath);
                word.setWordAudioPath(_wordAudioPath);
                word.setSyllableFinished(_syllableFinished);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(database!=null){
                database.close();
            }
        }
        return word;

    }
}
