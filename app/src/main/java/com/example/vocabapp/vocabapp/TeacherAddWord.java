package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.Model.ViewWord;

import org.json.JSONArray;

import java.util.ArrayList;

public class TeacherAddWord extends AppCompatActivity {

    Button saveChange;
    ImageButton goBack;
    EditText et_word,et_syllable;
    TextView et_wordCount;
    private final String url = "http://130.61.29.169:8080/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_teacher_add_word);
        saveChange = findViewById(R.id.AddWord_saveChange);
        goBack = findViewById(R.id.AddWord_goBack);
        et_word = findViewById(R.id.AddWord_word);
        et_syllable = findViewById(R.id.AddWord_syllable);
        et_wordCount = findViewById(R.id.AddWord_numOfWords);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TeacherAddWord.this,ViewWord.class);
                startActivity(intent);
            }
        });
        saveChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty()) {
                    Toast.makeText(TeacherAddWord.this,"All fields can not be empty",Toast.LENGTH_SHORT).show();
                } else if(getTopicWordCount() >= 10) {
                    Toast.makeText(TeacherAddWord.this,"No more than 10 words in one topic",Toast.LENGTH_SHORT).show();
                } else {
                    saveChange();
                }
            }
        });
        et_wordCount.setText("Number of words in this topic:" + getTopicWordCount().toString());
    }

    private boolean isEmpty() {
        String word = et_word.getText().toString().trim();
        String syllable = et_syllable.getText().toString().trim();
        if(TextUtils.isEmpty(word) || TextUtils.isEmpty(syllable)) {
            return true;
        } else {
            return false;
        }
    }

    private Integer getTeacherId() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Teacher_ID = "Key_TeacherId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer id = sharedPreferences.getInt(Teacher_ID,0);
        return id;
    }

    private Integer getTopicId() {
        final String TOPIC_ID = "Key_TopicId";
        final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_VIEWTOPIC,MODE_PRIVATE);
        Integer id = sharedPreferences.getInt(TOPIC_ID,0);
        return id;
    }


    private Integer getTopicWordCount() {
        final String TopicWordCount = "Key_TopicWordCount";
        final String Preference_GetWordCount = "Preference_WordCount";
        SharedPreferences sharedPreferences = getSharedPreferences(Preference_GetWordCount,MODE_PRIVATE);
        Integer count = sharedPreferences.getInt(TopicWordCount,0);
        return count;
    }


    private void saveChange() {
        final Integer topicID = getTopicId();
        Integer TeacherID = getTeacherId();
        String word = et_word.getText().toString().trim();
        String syllable = et_syllable.getText().toString().trim();
        final String URL = url + "word/" + "add?" + "name=" + word + "&" + "syllable=" + syllable + "&" + "topicId=" + topicID + "&" + "teacherId=" + TeacherID;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("Saved")) {
                    Toast.makeText(TeacherAddWord.this,"Word added",Toast.LENGTH_SHORT).show();
                    incrementWord incrementWord = new incrementWord();
                    incrementWord.execute(topicID.toString());
                    Intent intent = new Intent(TeacherAddWord.this, ViewWord.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(TeacherAddWord.this,"Failed with duplicate word",Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TeacherAddWord.this,"Request failed",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(TeacherAddWord.this);
        requestQueue.add(stringRequest);

    }

    //After creating a new word the word count of the topic will increase by 1
    private  class incrementWord extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            final String URL = url + "topic/wordIncrement?" + "id=" + strings[0];
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(TeacherAddWord.this,"Request failed",Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(TeacherAddWord.this);
            requestQueue.add(stringRequest);
            return null;
        }
    }
}
