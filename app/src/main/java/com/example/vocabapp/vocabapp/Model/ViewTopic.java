package com.example.vocabapp.vocabapp.Model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.vocabapp.vocabapp.Activity_signup;
import com.example.vocabapp.vocabapp.Entity.Teacher;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.GameMain;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.MainActivity;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;
import com.example.vocabapp.vocabapp.TeacherAddTopic;
import com.example.vocabapp.vocabapp.Teacher_Home;

import org.json.JSONArray;

import java.util.ArrayList;

public class ViewTopic extends AppCompatActivity {

    private static final String Teacher_ID = "Key_TeacherId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    private static final String TAG = "ViewTopic";

    private ImageButton goBack;
    private SwipeMenuListView listView;
    private ImageView toHome,addTopic;
    private ArrayList<Topic> topics;
    private String topicToDisplay;
    private Integer if_game_start;
    MyDatabaseHelper dbHelper;
    ArrayAdapter<String> adapter;
    ArrayList<String> list;
    SharedPreferences userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_topic);

        addTopic = findViewById(R.id.ViewTopic_addTopic);
        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        goBack = findViewById(R.id.ViewTopic_goBack);
        toHome = findViewById(R.id.ViewTopic_goHome);
        listView = (SwipeMenuListView) findViewById(R.id.listView);
        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer teacherId = userInfo.getInt(Teacher_ID,0);
        Intent intent = getIntent();
        if_game_start = intent.getIntExtra("game_page_start",0);

        displayAddButton();
        generateListView(teacherId);
        onClickListView();

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTeacher()) {
                    Intent intent = new Intent(ViewTopic.this,Teacher_Home.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(ViewTopic.this,Student_Home.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTeacher()) {
                    Intent intent = new Intent(ViewTopic.this,Teacher_Home.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(ViewTopic.this,Student_Home.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        addTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTeacher()) {
                    Intent intent = new Intent(ViewTopic.this,TeacherAddTopic.class);
                    startActivity(intent);
                    finish();
                }
            }
        });


        if(isTeacher()) {
//             change the icons from the child ones to adult ones
            toHome.setImageResource(R.drawable.homebuttonteacher);
            goBack.setImageResource(R.drawable.whitearrow);



            //toHome.setLayoutParams();
            SwipeMenuCreator creator = new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    // create "delete" item
                    SwipeMenuItem deleteItem = new SwipeMenuItem(
                            getApplicationContext());
                    // set item background
                    deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                            0x3F, 0x25)));
                    // set item width
                    deleteItem.setWidth(220);
                    // set a icon
                    deleteItem.setIcon(R.drawable.ic_delete_);
                    // add to menu
                    menu.addMenuItem(deleteItem);
                }
            };
            Toast.makeText(ViewTopic.this,"Click on a topic to add words",Toast.LENGTH_LONG).show();
            listView.setMenuCreator(creator);
            listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                String url = userInfo.getString(BASIC_URL, null);

                @Override
                public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            // delete
                            Integer topic_ID = topics.get(position).getId();
                            Log.d(TAG, "onMenuItemClick: position: "+ position);
                            Log.d(TAG, "onMenuItemClick: topic id: "+ topic_ID);
                            String URL = url + "topic/deleteById?id=" + topic_ID;
                            String URL_deleteWord = url + "word/deleteByTopicId?id=" + topic_ID;
                            ClearWordTask clearWordTask = new ClearWordTask();
                            clearWordTask.execute(URL_deleteWord);

                            RequestQueue queue = Volley.newRequestQueue(ViewTopic.this);
                            StringRequest stringRequest2 = new StringRequest(Request.Method.DELETE, URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            if (response.equals("Failed")) {
                                                Toast.makeText(ViewTopic.this, "Delete topic failed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(ViewTopic.this, "Delete", Toast.LENGTH_SHORT).show();
                                                list.remove(position);
                                                topics.remove(position);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(ViewTopic.this, "Request delete topics failed", Toast.LENGTH_SHORT).show();

                                }
                            });
                            queue.add(stringRequest2);
                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });
        }
    }


    // set creator

    private void displayAddButton() {
        if(isTeacher()) {
            addTopic.setVisibility(View.VISIBLE);
        } else {
            addTopic.setVisibility(View.GONE);
        }
    }


    private boolean isTeacher() {
        if(userInfo.getBoolean(Identity,false)) {
            return true;
        } else {
            return false;
        }
    }

    //when the topic is deleted the words in that topic will be deleted accordingly
    private class ClearWordTask extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            StringRequest stringRequest1 = new StringRequest(Request.Method.DELETE, strings[0],
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.equals("Failed")) {
//                                Toast.makeText(ViewTopic.this, "Delete topic failed", Toast.LENGTH_SHORT).show();
                            } else {

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            RequestQueue queue = Volley.newRequestQueue(ViewTopic.this);
            queue.add(stringRequest1);
            return null;
        }
    }

    //Getting all the topics associated to the teacher and display on the listview
    private void generateListView(Integer teacherId) {
        Integer id = teacherId;
        String url = userInfo.getString(BASIC_URL,null);
        String URL = url + "topic/getTopicByTeacherId?" + "id=" + id;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        list = new ArrayList<>();
                        topics = Topic.fromJson(response);
                        for(Topic topic : topics) {
                            list.add(topic.getName());
                        }
                        TextView textView = findViewById(R.id.CustomTextView);
                        adapter = new ArrayAdapter<String>(ViewTopic.this,R.layout.customlayout,R.id.CustomTextView,list);
                        listView.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue queue = Volley.newRequestQueue(ViewTopic.this);
        queue.add(jsonArrayRequest);
    }


    private void onClickListView() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences;
                final String TOPIC_ID = "Key_TopicId";
                final String TOPIC_NAME = "Key_TopicName";
                final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
                sharedPreferences = getSharedPreferences(PREFERENCES_VIEWTOPIC,Context.MODE_PRIVATE);
                //store the information needed for the next page(topicId and topicName)
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Integer topic_ID = topics.get(i).getId();
                String topic_Name = topics.get(i).getName();
                editor.putInt(TOPIC_ID,topic_ID);
                editor.putString(TOPIC_NAME,topic_Name);
                editor.apply();
                Log.d(TAG, "onItemClick: topic_name = " + topic_Name);
                Log.d(TAG, "onItemClick: wordsFinished =" + wordsFinished(topic_Name));
                Log.d(TAG, "onItemClick: topics = " + topics.get(i).getWordCount());
                if (if_game_start == 1){
                    if(wordsFinished(topic_Name) <= 0.4 * topics.get(i).getWordCount()){
                        Toast.makeText(ViewTopic.this, "You have not finished half of this topic", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(ViewTopic.this,GameMain.class);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(ViewTopic.this,ViewWord.class);
                    startActivity(intent);
                }

            }
        });
    }
    Integer wordsFinished(String topicName){
        Integer wordsFinished = 0;
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from Word where topic=?", new String[]{topicName});
        if(cursor.moveToFirst()){
            do{
                int activityFinished = 0;
                Word word = new Word();
                String rhyme = cursor.getString(cursor.getColumnIndex("rhyme"));
                String sentence = cursor.getString(cursor.getColumnIndex("sentence"));
                String category = cursor.getString(cursor.getColumnIndex("category"));
                String lookAndFeel = cursor.getString(cursor.getColumnIndex("lookAndFeel"));
                String association = cursor.getString(cursor.getColumnIndex("association"));
                String audioPath = cursor.getString(cursor.getColumnIndex("audioPath"));
                String wordAudioPath = cursor.getString(cursor.getColumnIndex("wordAudioPath"));
                String videoPath = cursor.getString(cursor.getColumnIndex("videoPath"));
                String photoPath = cursor.getString(cursor.getColumnIndex("photoPath"));
                int syllableFinished = cursor.getInt(cursor.getColumnIndex("syllableFinished"));
                if (!rhyme.isEmpty()) activityFinished++;
                if (!sentence.isEmpty()) activityFinished++;
                if (!category.isEmpty()) activityFinished++;
                if (!lookAndFeel.isEmpty()) activityFinished++;
                if (!association.isEmpty()) activityFinished++;
                if (!audioPath.isEmpty()) activityFinished++;
                if (!wordAudioPath.isEmpty()) activityFinished++;
                if (!videoPath.isEmpty()) activityFinished++;
                if (!photoPath.isEmpty()) activityFinished++;
                if (syllableFinished != 0) activityFinished++;
                if (activityFinished == 10){
                    wordsFinished++;
                }

            } while (cursor.moveToNext());

        }
        return wordsFinished;
    }


}
