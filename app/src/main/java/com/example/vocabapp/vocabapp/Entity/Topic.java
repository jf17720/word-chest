package com.example.vocabapp.vocabapp.Entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Topic {

    private Integer id;

    private String name;

    private List<Word> words;

    private Integer teacherId;

    private Integer wordCount;


    public Integer getWordCount() {
        return wordCount;
    }

    public void setWordCount(Integer wordCount) {
        this.wordCount = wordCount;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setWords(List<Word> words) {

        this.words = words;
    }

    public void setTeacherId(Integer teacherId) {

        this.teacherId = teacherId;
    }

    public String getName() {

        return name;
    }

    public List<Word> getWords() {

        return words;
    }

    public Integer getTeacherId() {

        return teacherId;
    }

    public Integer getId() {

        return id;
    }

    //Convert from json object to java object
    public static Topic fromJson(JSONObject jsonObject) {
        Topic topic = new Topic();

        try {
            topic.id = jsonObject.getInt("id");
            topic.name = jsonObject.getString("name");
            topic.wordCount = jsonObject.getInt("wordCount");
            topic.teacherId = jsonObject.getInt("teacherId");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return topic;
    }

    //Convert from json array to java array of objects
    public static ArrayList<Topic> fromJson(JSONArray jsonArray) {

        JSONObject topicJson;
        ArrayList<Topic> topics = new ArrayList<Topic>(jsonArray.length());

        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                topicJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Topic topic = Topic.fromJson(topicJson);
            if(topic != null) {
                topics.add(topic);
            }
        }
        return topics;

    }
}
