package com.example.vocabapp.vocabapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Student;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Model.ViewStudent;
import com.example.vocabapp.vocabapp.Model.ViewTopic;
import com.example.vocabapp.vocabapp.Model.ViewWord;

import org.json.JSONArray;

import java.util.ArrayList;

public class Teacher_TrackProgress extends AppCompatActivity {
    SharedPreferences userInfo;
    private static final String Teacher_ID = "Key_TeacherId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    ListView listView;
    private ArrayList<Student> students;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher__track_progress);
        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        listView = findViewById(R.id.ViewStudent_listView_progress);
        Integer teacherId = userInfo.getInt(Teacher_ID,0);
        generateListView(teacherId);
        onClickListView();


        ImageButton backButton = (ImageButton) findViewById(R.id.backFromTeacherTrackProgress);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Teacher_TrackProgress.this, Teacher_Home.class);
                startActivity(intent);
            }

        });
    }

    private void generateListView(Integer teacherId) {
        String id = teacherId.toString();
        String url = userInfo.getString(BASIC_URL,null);
        String URL = url + "student/getStudentByTeacherId?" + "id=" + id;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<String> list = new ArrayList<>();
                        ArrayList<Student> students = new ArrayList<>();
                        students = Student.fromJson(response);
                        for(Student student : students) {
                            list.add(student.getName().toLowerCase());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Teacher_TrackProgress.this,R.layout.customlayout,R.id.CustomTextView,list);
                        listView.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue queue = Volley.newRequestQueue(Teacher_TrackProgress.this);
        queue.add(jsonArrayRequest);
    }
    private void onClickListView() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences;
                final String STUDENT_ID = "Key_StudentId";
                final String STUDENT_NAME = "Key_StudentName";
                final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
                sharedPreferences = getSharedPreferences(PREFERENCES_VIEWTOPIC,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Integer student_ID = students.get(i).getId();
                String student_Name = students.get(i).getName();
                editor.putInt(STUDENT_ID,student_ID);
                editor.putString(STUDENT_NAME,student_Name);
                editor.apply();
                Intent intent = new Intent(Teacher_TrackProgress.this,ViewTopic.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
