package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Teacher;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Model.StudentProgress;
import com.example.vocabapp.vocabapp.Model.ViewStudent;
import com.example.vocabapp.vocabapp.Model.ViewTopic;
import com.example.vocabapp.vocabapp.Tools.ProgressAdaptor;

import org.json.JSONArray;

import java.net.Inet4Address;
import java.net.URL;
import java.util.ArrayList;

public class Teacher_Home extends AppCompatActivity implements View.OnClickListener{

    Button btn_viewStudent,btn_viewTopic,btn_trackProgress;
    ImageButton logout;
    SharedPreferences userInfo;
    private Integer wordCount;
    private static final String Teacher_ID = "Key_TeacherId";
    private static final String PREFERENCES_StoreId = "Preference_Id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher__home);

        btn_viewStudent = findViewById(R.id.Button_viewStudent);
        btn_viewTopic = findViewById(R.id.Button_viewTopic);
        btn_trackProgress = findViewById(R.id.Button_trackProgress);
        logout = findViewById(R.id.backFromTeacherTrackProgress2);
        btn_viewStudent.setOnClickListener(this);
        btn_viewTopic.setOnClickListener(this);
        btn_trackProgress.setOnClickListener(this);
        logout.setOnClickListener(this);
        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer teacherId = userInfo.getInt(Teacher_ID,0);
        GetWordCountTask getWordCountTask = new GetWordCountTask();
        getWordCountTask.execute(String.valueOf(teacherId));
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.Button_viewTopic:
                Intent intent = new Intent(Teacher_Home.this, ViewTopic.class);
                startActivity(intent);
                finish();
                break;
            case R.id.Button_viewStudent:
                Intent intent1 = new Intent(Teacher_Home.this, ViewStudent.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.backFromTeacherTrackProgress2:
                Intent intent2 = new Intent(Teacher_Home.this,MainActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.Button_trackProgress:
                Intent intent3 = new Intent(Teacher_Home.this,StudentProgress.class);
                startActivity(intent3);
                finish();
                break;

        }
    }

    //get the number of total word count which will be used in other activities(for tracking progress)
    private class GetWordCountTask extends AsyncTask<String, String, String> {

        private static final String TotalWordCount = "Key_TotalWordCount";
        private static final String Preference_GetWordCount = "Preference_WordCount";
        SharedPreferences sharedPreferences;

        @Override
        protected String doInBackground(String... strings) {
            final String URL = "http://130.61.29.169:8080/word/getWordByTeacherId?" + "id=" + strings[0];
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                            new Response.Listener<JSONArray>() {
                                @Override
                                public void onResponse(JSONArray response) {
                                    wordCount = response.length();
                                    sharedPreferences = getSharedPreferences(Preference_GetWordCount,MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putInt(TotalWordCount,wordCount);
                                    editor.apply();
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            RequestQueue rQueue = Volley.newRequestQueue(Teacher_Home.this);
            rQueue.add(jsonArrayRequest);
            return null;
        }
    }
}
