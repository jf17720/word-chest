package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Teacher;
import com.example.vocabapp.vocabapp.Tools.AESUtils;
import com.example.vocabapp.vocabapp.Tools.SendMail;

import org.json.JSONArray;

import java.util.ArrayList;

public class ForgetPassword extends AppCompatActivity {

    private EditText et_email,et_name,et_usernmae;
    private Button btn_submit;
    private ImageButton btn_backToLogin;
    private String name,username,email;
    private String password = ""; //initialise the password to be blank
    Boolean flag = false; //true if the input match the data in database
    private static final String TAG = "ForgerPassword";
    private final String url = "http://130.61.29.169:8080/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        et_email = findViewById(R.id.forgetPass_email);
        et_name = findViewById(R.id.forgetPass_name);
        et_usernmae = findViewById(R.id.forgetPass_username);
        btn_submit = findViewById(R.id.forgetPass_submit);
        btn_backToLogin = findViewById(R.id.forgetPass_goBack);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = et_name.getText().toString().trim();
                username = et_usernmae.getText().toString().trim();
                checkInput checkInput = new checkInput();
                checkInput.execute(name,username);
                if(flag) {
                    sendEmail();
                }
            }
        });
        btn_backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ForgetPassword.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private void sendEmail() {


        //Getting content for email
        String email = et_email.getText().toString().trim();
        String subject = "*Retrieve Password*";
        String message = "Your password is:" + password;

        //Creating SendMail object
        SendMail sm = new SendMail(this, email, subject, message);

        //Executing sendmail to send email
        sm.execute();

    }

    //decrypt the password
    private String decrypted(String encrypted) {
        String decrypted = "";
        try {
            decrypted = AESUtils.decrypt(encrypted);
            Log.d("TEST", "decrypted:" + decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decrypted;
    }

    //check if the info input match or not
    private class checkInput extends AsyncTask<String,String,String> {

        private final String teacherURL = url + "teacher/" + "all";

        @Override
        protected String doInBackground(final String... strings) {
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, teacherURL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            ArrayList<Teacher> teachers = new ArrayList<>(response.length());
                            teachers = Teacher.fromJson(response);
                            for (int i = 0; i < teachers.size(); i++) {
                                if (teachers.get(i).getUsername().equals(strings[1]) && teachers.get(i).getName().equalsIgnoreCase(strings[0])) {
                                    flag = true;
                                    password = decrypted(teachers.get(i).getPassword());
                                }
                            }
                            if(!flag) {
                                Toast.makeText(ForgetPassword.this, "The information you've entered is wrong,Please check again", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast toast = Toast.makeText(ForgetPassword.this, "request failed", Toast.LENGTH_SHORT);
                    toast.show();
                }
            });
            RequestQueue rQueue = Volley.newRequestQueue(ForgetPassword.this);
            rQueue.add(jsonArrayRequest);
            return null;
        }
    }
}
