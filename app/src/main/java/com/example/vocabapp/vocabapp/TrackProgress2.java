package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup.LayoutParams;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Model.ViewTopic;
import com.example.vocabapp.vocabapp.Model.ViewWord;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrackProgress2 extends AppCompatActivity {
    //new list holding all the topics that a student has
    List<TrackProgress2.Topic_Progress> topics_list = new ArrayList<>();

    long id;
    int j = 0;
    int i = 0;
    int tempwhitebox= 0; //the white box behind the coins
    int lastComplete;
    int numberCompletedBefore;
    MyDatabaseHelper dbHelper;
    SharedPreferences userInfo,topicInfo;
    private ArrayList<Topic> topicss;
    Map<String,Integer> topic_wordCount = new HashMap<>();

    private static final String Teacher_ID = "Key_TeacherId";
    private static final String TOPIC_ID = "Key_TopicId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    private static final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
    int wordCount = -1;
    private static final String TAG = "TrackProgress2";
    int lock =0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_progress2);

        //when back button is pressed, users must be returned to the previous page (home)
        ImageButton backButton = (ImageButton) findViewById(R.id.backFromTrackProgress);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TrackProgress2.this, Student_Home.class);
                startActivity(intent);
            }

        });
        //home button must return users to the home page
        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromTrackProgress);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TrackProgress2.this, Student_Home.class);
                startActivity(intent);
            }

        });


        loadTopicWordCount();




    }

    public int loadTopicWordCount(){
        userInfo = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        topicInfo = getSharedPreferences(PREFERENCES_VIEWTOPIC,MODE_PRIVATE);
        String url = userInfo.getString(BASIC_URL, null);
        Integer teacherId = userInfo.getInt(Teacher_ID, 0);
        final String URL_topic = url + "topic/getTopicByTeacherId?" + "id=" + teacherId;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL_topic, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<Topic> topics = Topic.fromJson(response);
                        for(Topic topic : topics) {
                            topic_wordCount.put(topic.getName(),topic.getWordCount());
                        }
                        for(Map.Entry<String,Integer> entry : topic_wordCount.entrySet()){
                           topics_list.add(new Topic_Progress(entry.getKey(),wordsFinished(entry.getKey()),entry.getValue()-wordsFinished(entry.getKey())));
                        }
                        // Setting the RelativeLayout as our content view
                        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.theLayout);
                        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT,
                                RelativeLayout.LayoutParams.MATCH_PARENT);
                        int widthOfParent = relativeLayout.getWidth(); //find the width of the phone or tablet they are using
                        int sizeOfBox = (widthOfParent * 14)/16; //set the size of the white boxes accordingly
                        int leftMargin = (widthOfParent/16); //set the size of the white boxes accordingly
                        int coinLeftMargin= leftMargin +5; //set the margin of the coins accordingly
                        int[] idArray = new int[topics_list.size()]; //list of the topics
                        int[] idArray2 = new int[20];
                        int[] idArray3 = new int[20];
                        for (TrackProgress2.Topic_Progress t : topics_list //go through the topic list
                                ) {

//                            set the layout paramters of the text (regarding the topics)
                            TextView ivBowl = new TextView(TrackProgress2.this);
                            ivBowl.setText(t.name);
                            ivBowl.setTextSize(20);
                            ivBowl.setGravity(Gravity.CENTER_HORIZONTAL);
                            ivBowl.setTextColor(Color.BLACK);

//                          give the topic text a unique id
                            int hello = ViewCompat.generateViewId();
                            ivBowl.setId(hello);
                            idArray[j] = hello;


                            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//                          if the current topic is not the first one in the list
                            if (j != 0) {
//                                put the topic word below the white box above
                                lp.setMargins(coinLeftMargin, 60, 0, 0);
                                lp.addRule(RelativeLayout.BELOW, tempwhitebox );
                                ivBowl.setLayoutParams(lp);
                                relativeLayout.addView(ivBowl);

                            } else { // if this topic is the first one in the list
                                // Adding the TextView to the RelativeLayout as a child
                                lp.setMargins(coinLeftMargin, 200, 0, 0);
                                //lp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                                ivBowl.setLayoutParams(lp);
                                relativeLayout.addView(ivBowl);
                            }

                            numberCompletedBefore = t.numberComplete;

//                            set the white box to be a light blue  box from images
                            ImageView whiteBox= new ImageView(TrackProgress2.this);
                            whiteBox.setBackgroundResource(R.drawable.lightbluebox);

//                          height of box
                            RelativeLayout.LayoutParams whiteBoxLayout = new RelativeLayout.LayoutParams(sizeOfBox,90);
                            whiteBoxLayout.setMargins(leftMargin,0,0,0);
//                          put the white box below the currently added topic
                            whiteBoxLayout.addRule(RelativeLayout.BELOW, idArray[j]);
                            whiteBox.setLayoutParams(whiteBoxLayout);
                            relativeLayout.addView(whiteBox);

//                          give the white box a unique id
                            hello = ViewCompat.generateViewId();
                            whiteBox.setId(hello);
                            tempwhitebox = hello;

//                          add the gold coins
                            for(int i=0; i<(t.numberComplete); i++){
                                ImageView imageComplete= new ImageView(TrackProgress2.this);
                                imageComplete.setBackgroundResource(R.drawable.coin);
                                RelativeLayout.LayoutParams theCoinLayout = new RelativeLayout.LayoutParams(90,90);
                                if (i==0){ //if this is the first gold coin
                                    theCoinLayout.setMargins(coinLeftMargin,0,0,0);
                                    theCoinLayout.addRule(RelativeLayout.BELOW, idArray[j]);
                                    imageComplete.setLayoutParams(theCoinLayout);
                                    relativeLayout.addView(imageComplete);
                                }else{ // if this is not the first gold coin then place it right of the one before
                                    theCoinLayout.addRule(RelativeLayout.BELOW, idArray[j]);
                                    theCoinLayout.addRule(RelativeLayout.RIGHT_OF, idArray2[(i-1)]);
                                    imageComplete.setLayoutParams(theCoinLayout);
                                    relativeLayout.addView(imageComplete);
                                }
//                               give the last gold coin a unqiue id
                                int tempk = ViewCompat.generateViewId();
                                imageComplete.setId(tempk);
                                idArray2[i]= tempk;
                                lastComplete = i;
                            }

//                              add the grey coins for uncompleted words in a topic
                            for(int k=0; k<(t.numberNotComplete); k++){
                                ImageView imageNotComplete= new ImageView(TrackProgress2.this);
                                imageNotComplete.setBackgroundResource(R.drawable.coingrey);
                                RelativeLayout.LayoutParams theGreyCoinLayout = new RelativeLayout.LayoutParams(90,90);
                                if ((k==0) && (t.numberComplete == 0)){ // adding a grey coin if its the first and there are no gold coins at all
                                    theGreyCoinLayout.setMargins(coinLeftMargin,0,0,0);
                                    theGreyCoinLayout.addRule(RelativeLayout.BELOW, idArray[j]);
                                    imageNotComplete.setLayoutParams(theGreyCoinLayout);
                                    relativeLayout.addView(imageNotComplete);


                                }else if (k==0) { //adding a grey coin if its the first and there are gold coins
                                    theGreyCoinLayout.addRule(RelativeLayout.BELOW, idArray[j]);
                                    theGreyCoinLayout.addRule(RelativeLayout.RIGHT_OF, idArray2[lastComplete]);
                                    imageNotComplete.setLayoutParams(theGreyCoinLayout);
                                    relativeLayout.addView(imageNotComplete);

                                }else{ //if its not the first grey coin

                                    theGreyCoinLayout.addRule(RelativeLayout.BELOW, idArray[j]);
                                    theGreyCoinLayout.addRule(RelativeLayout.RIGHT_OF, idArray3[(k-1)]);
                                    imageNotComplete.setLayoutParams(theGreyCoinLayout);
                                    relativeLayout.addView(imageNotComplete);
                                }
//                              give the grey coins a unique id
                                int tempk = ViewCompat.generateViewId();
                                imageNotComplete.setId(tempk);
                                idArray3[k]= tempk;
                            }
                            j++;

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: error");
            }
        });
        RequestQueue queue = Volley.newRequestQueue(TrackProgress2.this);
        queue.add(jsonArrayRequest);
        return 1;
    }





// find out how many words in a topic have been completed
    Integer wordsFinished(String topicName){
        Integer wordsFinished = 0;
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from Word where topic=?", new String[]{topicName});
        if(cursor.moveToFirst()){
            do{
                int activityFinished = 0;
                String rhyme = cursor.getString(cursor.getColumnIndex("rhyme"));
                String sentence = cursor.getString(cursor.getColumnIndex("sentence"));
                String category = cursor.getString(cursor.getColumnIndex("category"));
                String lookAndFeel = cursor.getString(cursor.getColumnIndex("lookAndFeel"));
                String association = cursor.getString(cursor.getColumnIndex("association"));
                String audioPath = cursor.getString(cursor.getColumnIndex("audioPath"));
                String wordAudioPath = cursor.getString(cursor.getColumnIndex("wordAudioPath"));
                String videoPath = cursor.getString(cursor.getColumnIndex("videoPath"));
                String photoPath = cursor.getString(cursor.getColumnIndex("photoPath"));
                int syllableFinished = cursor.getInt(cursor.getColumnIndex("syllableFinished"));
                if (!rhyme.isEmpty()) activityFinished++;
                if (!sentence.isEmpty()) activityFinished++;
                if (!category.isEmpty()) activityFinished++;
                if (!lookAndFeel.isEmpty()) activityFinished++;
                if (!association.isEmpty()) activityFinished++;
                if (!audioPath.isEmpty()) activityFinished++;
                if (!wordAudioPath.isEmpty()) activityFinished++;
                if (!videoPath.isEmpty()) activityFinished++;
                if (!photoPath.isEmpty()) activityFinished++;
                if (syllableFinished != 0) activityFinished++;
                if (activityFinished == 10){
                    wordsFinished++;
                }
            } while (cursor.moveToNext());

        }
        return wordsFinished;
    }


    private class Topic_Progress {
        int numberComplete;
        int numberNotComplete;
        String name;

        Topic_Progress(String title, int complete, int notComplete) {
            numberComplete = complete;
            numberNotComplete = notComplete;
            name = title;
        }
    }
}

