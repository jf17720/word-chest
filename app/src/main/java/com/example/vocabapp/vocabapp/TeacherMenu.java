package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Button;
public class TeacherMenu extends AppCompatActivity {

    SharedPreferences userInfo;
    public static final String UserId = "Key_UserId";
    public static final String PREFERENCES_StoreId = "Preference_Id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_menu);

        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer id = userInfo.getInt(UserId,0);

        Button addStudentButton = findViewById(R.id.buttonTeacherAddNewStudent);
        addStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TeacherMenu.this,StudentRegister.class);
                startActivity(intent);
            }
        });

        Button addWordButton = (Button) findViewById(R.id.ButtonTeacherAddNewWords);
        addWordButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TeacherMenu.this, TeacherAddWord.class);
                startActivity(intent);
            }

        });

        Button addTopicButton = (Button) findViewById(R.id.ButtonTeacherAddNewTopic);
        addTopicButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TeacherMenu.this, TeacherAddTopic.class);
                startActivity(intent);
            }

        });

        Button trackButton = (Button) findViewById(R.id.buttonTeacherTrackProgress);
        trackButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TeacherMenu.this, Teacher_TrackProgress.class);
                startActivity(intent);
            }

        });

        Button viewStudentButton = findViewById(R.id.ButtonTeacherViewStudent);
        viewStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}
