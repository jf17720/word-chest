package com.example.vocabapp.vocabapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Student;
import com.example.vocabapp.vocabapp.Entity.Teacher;
import com.example.vocabapp.vocabapp.Model.WordProgress;
import com.example.vocabapp.vocabapp.Tools.AESUtils;

import org.json.JSONArray;

import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,CompoundButton.OnCheckedChangeListener {

    Button btn_teacher_signin,btn_student_signin,btn_signup;
    EditText et_uname, et_pass;
    CheckBox checkBox;
    String name, username, password;
    TextView textview1,textView2,textView3,textView4,forgetPass;
    SharedPreferences sharedPreferences;
    private final String url = "http://130.61.29.169:8080/";
    private static final String PREFERENCES_RememberPass = "Preference_RememberPass" ;//this preference is for remembering password
    private static final String PREFERENCES_StoreId = "Preference_Id"; //this preference is for storing user information
    private static final String Username = "Key_Username";
    private static final String Password = "Key_Password";
    private static final String RememberMe = "Remember_Pass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_student_signin = findViewById(R.id.Btn_student_signin);
        btn_teacher_signin = findViewById(R.id.Btn_teacher_signin);
        btn_signup = findViewById(R.id.Btn_signup);
        et_uname = findViewById(R.id.et_uname);
        et_pass = findViewById(R.id.et_pass);
        checkBox = findViewById(R.id.checkBox_main);
        forgetPass = findViewById(R.id.textView_main);
        sharedPreferences = getSharedPreferences(PREFERENCES_RememberPass,Context.MODE_PRIVATE);
        btn_student_signin.setOnClickListener(this);
        btn_teacher_signin.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        forgetPass.setOnClickListener(this);

        boolean check = sharedPreferences.getBoolean(RememberMe,false);
        checkBox.setChecked(check);
        checkBox.setOnCheckedChangeListener(this);

        //get the pre-stored password
        if(check) {
            String name = sharedPreferences.getString(Username,null);
            String pass = sharedPreferences.getString(Password,null);
            et_uname.setText(name);
            et_pass.setText(pass);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        //if the check box for remember password is clicked that store the state
        SharedPreferences sp = getSharedPreferences(PREFERENCES_RememberPass, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        if(isChecked) {
            editor.putBoolean(RememberMe, true);
        } else {
            editor.remove(RememberMe);
        }
        editor.apply();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Btn_teacher_signin:
                username = et_uname.getText().toString().trim();
                password = et_pass.getText().toString().trim();
                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    Toast.makeText(this, "Username or Password cannot be empty", Toast.LENGTH_SHORT).show();

                }
                else {
                    String identifier = "teacher";
                    LoginCheckTask loginCheckTask = new LoginCheckTask();
                    loginCheckTask.execute(username, password,identifier);
                    }

                break;
            case R.id.Btn_student_signin:
                username = et_uname.getText().toString().trim();
                password = et_pass.getText().toString().trim();
                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    Toast.makeText(this, "Username or Password cannot be empty", Toast.LENGTH_SHORT).show();

                }
                else {
                    String identifier = "student";
                    LoginCheckTask loginCheckTask = new LoginCheckTask();
                    loginCheckTask.execute(username, password,identifier);
                }
                break;
            case R.id.textView_main:
                Intent intent_forgetPass = new Intent(this,ForgetPassword.class);
                startActivity(intent_forgetPass);
                finish();
                break;
            case R.id.Btn_signup:
                Intent intent_register = new Intent(this, Activity_signup.class);
                startActivity(intent_register);
                finish();
                break;
        }
    }



    //used to decrypt the password
    private String decrypted(String encrypted) {
        String decrypted = "";
        try {
            decrypted = AESUtils.decrypt(encrypted);
            Log.d("TEST", "decrypted:" + decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decrypted;
    }


    private class LoginCheckTask extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;

        SharedPreferences userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        private static final String Teacher_ID = "Key_TeacherId";
        private static final String Student_ID = "Key_StudentId";
        private static final String Student_NAME = "Key_StudentName";
        private static final String Identity = "Key_Identity";
        private static final String BASIC_URL = "Key_Url";
        private final String teacherURL = url + "teacher/" + "all";
        private final String studentURL = url + "student/" + "all";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this,"Logging in","Please wait...",false,false);
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            //Dismissing the progress dialog
            progressDialog.dismiss();
        }


        @Override
        protected String doInBackground(String... strings) {

            final String username = strings[0];
            final String password = strings[1];

            if(checkBox.isChecked()) {
                SharedPreferences sp = getSharedPreferences(PREFERENCES_RememberPass,MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString(Username,strings[0]);
                editor.putString(Password,strings[1]);
                editor.apply();
            }

            if(strings[2].equals("teacher")) {
                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, teacherURL, null,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                ArrayList<Teacher> teachers = new ArrayList<>(response.length());
                                teachers = Teacher.fromJson(response);
                                boolean match = false;
                                for (int i = 0; i < teachers.size(); i++) {
                                    if (teachers.get(i).getUsername().equals(username) && decrypted((teachers.get(i).getPassword())).equals(password)) {
                                        match = true;
                                        //store the informatio of the teacher for future use
                                        SharedPreferences.Editor editor_Info = userInfo.edit();
                                        editor_Info.putInt(Teacher_ID,teachers.get(i).getId());
                                        editor_Info.putBoolean(Identity,true);
                                        editor_Info.putString(BASIC_URL,url);
                                        editor_Info.apply();
                                        Toast.makeText(MainActivity.this, "Log in successfully", Toast.LENGTH_SHORT).show();
                                        Intent intent1 = new Intent(MainActivity.this,Teacher_Home.class);
                                        startActivity(intent1);
                                        finish();
                                        break;
                                    }
                                }
                                if(!match) {
                                    Toast.makeText(MainActivity.this, "You have entered the wrong username or password", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast toast = Toast.makeText(MainActivity.this, "request failed", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
                RequestQueue rQueue = Volley.newRequestQueue(MainActivity.this);
                rQueue.add(jsonArrayRequest);
                return null;
            } else {

                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, studentURL, null,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                ArrayList<Student> students = new ArrayList<>(response.length());
                                students = Student.fromJson(response);
                                boolean match = false;
                                for (int i = 0; i < students.size(); i++) {
                                    if (students.get(i).getUsername().equals(username) && decrypted((students.get(i).getPassword())).equals(password)){
                                        match = true;
                                        //store both the information of the teacher and the student for future use
                                        SharedPreferences.Editor editor_Info = userInfo.edit();
                                        editor_Info.putInt(Teacher_ID,students.get(i).getTeacherId());
                                        editor_Info.putInt(Student_ID,students.get(i).getId());
                                        Log.d("this", "sssssstudentid=: "+students.get(i).getId());
                                        editor_Info.putString(Student_NAME,students.get(i).getName());
                                        editor_Info.putBoolean(Identity,false);
                                        editor_Info.putString(BASIC_URL,url);
                                        editor_Info.apply();
                                        Toast.makeText(MainActivity.this, "Log in successfully", Toast.LENGTH_SHORT).show();
                                        Intent intent1 = new Intent(MainActivity.this,Student_Home.class);
                                        startActivity(intent1);
                                        finish();
                                        break;
                                    }
                                }
                                if(!match) {
                                    Toast.makeText(MainActivity.this, "You have entered the wrong username or password", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast toast = Toast.makeText(MainActivity.this, "request failed", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
                RequestQueue rQueue = Volley.newRequestQueue(MainActivity.this);
                rQueue.add(jsonArrayRequest);
                return null;

            }

        }
    }
}
