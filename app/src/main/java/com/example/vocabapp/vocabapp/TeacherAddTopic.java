package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Model.ViewTopic;

import java.net.URL;

public class TeacherAddTopic extends AppCompatActivity {

    private Button saveChange;
    private EditText topic;
    ImageButton backButton;
    private final String url = "http://130.61.29.169:8080/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_add_topic);
        saveChange = findViewById(R.id.AddTopic_saveChange);
        topic = findViewById(R.id.AddTopic_topicContent);
        backButton = (ImageButton) findViewById(R.id.AddTopic_goBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TeacherAddTopic.this, ViewTopic.class);
                startActivity(intent);
            }

        });
        saveChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty()) {
                    Toast.makeText(TeacherAddTopic.this, "Topic can not be empty", Toast.LENGTH_SHORT).show();
                } else {
                    SaveTopic();
                }
            }
        });

    }

    private boolean isEmpty() {
        String content = topic.getText().toString().trim();
        if(TextUtils.isEmpty(content)) {
            return true;
        } else {
            return false;
        }
    }

    private void SaveTopic() {
        final String Teacher_ID = "Key_TeacherId";
        final String PREFERENCES_StoreId = "Preference_Id";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        Integer teacherID = sharedPreferences.getInt(Teacher_ID, 0);
        String name = topic.getText().toString().trim();
        String URL = url + "topic/" + "add?" + "name=" + name + "&" + "teacherId=" + teacherID;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("Saved")) {
                    Toast.makeText(TeacherAddTopic.this,"Topic added",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(TeacherAddTopic.this, ViewTopic.class);
                    startActivity(intent);
                } else { //duplicate topic is not allowed here
                    Toast.makeText(TeacherAddTopic.this,"Failed with duplicate topic",Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TeacherAddTopic.this,"request failed",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(TeacherAddTopic.this);
        requestQueue.add(stringRequest);
    }
}
