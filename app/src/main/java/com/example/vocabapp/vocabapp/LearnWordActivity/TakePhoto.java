package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;

import java.io.File;
import java.io.IOException;
import java.util.Date;

//
//Activity to take picture of the word
//
public class TakePhoto extends AppCompatActivity {
    File photoFile;
    Uri photoURI;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_IMAGE_GET =20;
    String mCurrentPhotoPath;
    ImageView imageView1;
    TextView showWord;
    private MyDatabaseHelper dbHelper;
    String theWord;
    final int REQUEST_PERMISSION_CODE = 1000;

    public static int TakenPhoto = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);
        Intent intent = getIntent();
        theWord = intent.getStringExtra("the_word");
        showWord = findViewById(R.id.showWord_photo);
        showWord.setText(theWord);
        if(!checkPermissionFromDevice()){
            requestPermission();
        }
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()});

        if(cursor.moveToFirst()){
            do{
                mCurrentPhotoPath = cursor.getString(cursor.getColumnIndex("photoPath"));
            } while (cursor.moveToNext());
            cursor.close();
        }
        imageView1 = findViewById(R.id.imageView);
        //Try to load latest photo from file system
        loadPhoto();

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromTakePhoto);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TakePhoto.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromTakePhoto);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TakePhoto.this, Student_Home.class);
                startActivity(intent);
            }

        });

        Button loadGallery = (Button) findViewById(R.id.galleryLoad);
        loadGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.print("Trying to load gallery");
                Intent intent2 = new Intent(Intent.ACTION_GET_CONTENT);
                intent2.setType("image/*");
                startActivityForResult(intent2, REQUEST_IMAGE_GET);
            }});

        //Take picture
        Button takePicture = (Button) findViewById(R.id.takePicture);
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }});

    }

    //Create unique file name
    private File createImageFile() throws IOException {
        System.out.println("Creating file .....");
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = image.getAbsolutePath();
//        System.out.println(mCurrentPhotoPath + '\n');
        return image;
    }

    //Create intent to use the users camera
    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                System.out.println(" **************************Error creating file");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                System.out.println(photoFile);
                photoURI = FileProvider.getUriForFile(this,
                        "com.example.vocabapp.vocabapp.fileprovider",
                        photoFile);
                final CRUD_interface word = new DatabaseController_Word( this,"WORD_STORE.db", null,1);
                Word sele = word.select(theWord,getStudentID());
                sele.setPhotoPath(photoFile.getAbsolutePath());
                word.update(sele);

                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission is granted");
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }


            }
        }
    }

    //On result of activities for intents save the photo from intent into database and display
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Finished result");
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            System.out.println("Got through if statement");
            addPhotoToGallery();
        }
        if (requestCode == REQUEST_IMAGE_GET && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            Uri fullPhotoUri = data.getData();
            Log.d("Getting Data", fullPhotoUri.getPath());
            try {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                imageView1.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                final CRUD_interface word = new DatabaseController_Word( this,"WORD_STORE.db", null,1);
                Word sele = word.select(theWord,getStudentID());
                sele.setPhotoPath(picturePath);
                word.update(sele);

                Log.d("ImagePath", picturePath);
            }  catch (Exception e)
            {}
        }
    }

    //Add photo to the gallery
    private void addPhotoToGallery() {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        System.out.println("Trying to save photo");
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(photoURI); //your file uri
        this.sendBroadcast(mediaScanIntent);
        if(photoFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
            Bitmap rotatedBitmap = Bitmap.createBitmap(myBitmap , 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
            imageView1.setImageBitmap(rotatedBitmap);
            if(TakenPhoto==0){
               TakenPhoto=1;
               Learnwords.red++;
            }
        }
        Toast.makeText(TakePhoto.this, "SAVED!", Toast.LENGTH_SHORT).show();
    }

    //Load photo from file system
    private void loadPhoto(){
        Matrix matrix = new Matrix();
        //matrix.postRotate(90);
//        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        mediaScanIntent.setData(photoURI); //your file uri
//        this.sendBroadcast(mediaScanIntent);
        if(!mCurrentPhotoPath.isEmpty()){
            Bitmap myBitmap = BitmapFactory.decodeFile( mCurrentPhotoPath);
            Log.d("testinsert",mCurrentPhotoPath);
            Log.d("testinsert",myBitmap.toString());
            if(myBitmap != null) {
//                Bitmap rotatedBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
                imageView1.setImageBitmap(myBitmap);
            }

        }

    }
    private Integer getStudentID() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }
    private  boolean checkPermissionFromDevice(){
        int wirte_external_storge_result = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        boolean ext_storage = (wirte_external_storge_result == PackageManager.PERMISSION_GRANTED);
        return ext_storage;
    }

}
