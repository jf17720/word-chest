package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;

import java.io.File;
import java.io.IOException;
import java.util.Date;

//
//Activity to record action
//

public class RecordAction extends AppCompatActivity {
    static final int REQUEST_VIDEO_CAPTURE = 100;
    VideoView mVideoView;
    String mCurrentVideoPath;
    Uri videoURI;
    File videoFile;
    public static int VideoTaken =0;
    MyDatabaseHelper dbHelper;
    String theWord;
    private TextView showWord;
    private String TAG = "RecordAction";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_action);

        Intent intent = getIntent();
        theWord = intent.getStringExtra("the_word"); //get the current word from previous activity
        showWord = findViewById(R.id.showWord_RecordAction);
        showWord.setText(theWord); // show the word in textview as a reminder
        mVideoView = findViewById(R.id.video);
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1); //get database object
        final SQLiteDatabase db = dbHelper.getWritableDatabase();//get writable database
        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()});
        //find the current video path from this row and update mCurrentVideoPath
        if(cursor.moveToFirst()){
            do{
                mCurrentVideoPath = cursor.getString(cursor.getColumnIndex("videoPath"));
            } while (cursor.moveToNext());
            cursor.close();
        }
        Log.d(TAG, "onCreate: database"+ mCurrentVideoPath);
        //go back to Learnwords interface
        ImageButton backButton = (ImageButton) findViewById(R.id.backFromRecordAction);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecordAction.this, Learnwords.class);
                startActivity(intent);
            }

        });
        //go bace to Home
        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromRecordAction);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecordAction.this, Student_Home.class);
                startActivity(intent);
            }

        });

        //Record video using users camera
        Button record = findViewById(R.id.recordAction);
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakeVideoIntent();
            }
        });

        //Play video
        Button playButton = (Button) findViewById(R.id.playVideo);
        playButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mVideoView.start();
            }

        });
        loadVideo();
        Log.d(TAG, "onCreate: 4");
    }

    //Dispatch intent to save video
    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            videoFile = null;
            try {
                videoFile = createVideoFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                System.out.println(" ********" +
                        "******************Error creating file");
            }
            // Continue only if the File was successfully created
            if (videoFile != null) {
                System.out.println(videoFile);
                videoURI = FileProvider.getUriForFile(this,
                        "com.example.vocabapp.vocabapp.fileprovider",
                        videoFile);
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission is granted");
                    takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI);
                    startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                    final CRUD_interface word = new DatabaseController_Word( this,"WORD_STORE.db", null,1);
                    Word sele = word.select(theWord,getStudentID());
                    sele.setVideoPath(videoFile.getAbsolutePath());
                    word.update(sele);
                }
            }
        }
    }

    private Integer getStudentID() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }

    //Create unique file name
    private File createVideoFile() throws IOException {
        System.out.println("Creating file .....");
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "MP4_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File video = File.createTempFile(
                imageFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentVideoPath = video.getAbsolutePath();
        System.out.println(mCurrentVideoPath + '\n');
        return video;
    }

    //Display video and save it if returned from intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(videoURI);
            this.sendBroadcast(mediaScanIntent);
            mVideoView.setVideoURI(videoURI);
            mVideoView.start();
            VideoTaken=1;
            Toast.makeText(RecordAction.this, "SAVED!", Toast.LENGTH_SHORT).show();
        }
    }

    //Load video from file system
    private void loadVideo(){
        String uriPath = mCurrentVideoPath;
        if(!mCurrentVideoPath.matches("")){
            File videoFile = new File(uriPath);
            Uri video = FileProvider.getUriForFile(this, "com.example.vocabapp.vocabapp.fileprovider", videoFile);
            Log.d("Video", video.toString());
            try{
                mVideoView.setVideoURI(video);
                mVideoView.start();
            } catch (Exception e){
                Log.d(TAG, "loadVideo: failed");
            }
        }


    }



}
