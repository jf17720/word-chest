package com.example.vocabapp.vocabapp;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Model.ViewTopic;

import org.json.JSONArray;

import java.util.ArrayList;

public class Student_Home extends AppCompatActivity {

    Button chooseTopic,game,trackProgress;
    ImageButton logout;
    MyDatabaseHelper dbHelper;
    ArrayList<Topic> topics = new ArrayList<>();
    boolean unlockgame = false;
    //TextView logout;
    AlarmManager alarmManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student__home);
        chooseTopic = findViewById(R.id.StudentHome_viewTopic);
        game = findViewById(R.id.StudentHome_game);
        trackProgress = findViewById(R.id.StudentHome_trackProgress);


        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        createNotificationChannel();
        //Schedules notification 3 days in advance
        scheduleNotification(getNotification(), 259200000);
        logout = findViewById(R.id.backFromStudentMenu);
        chooseTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Student_Home.this,ViewTopic.class);
                startActivity(intent);
            }
        });
        generateTopic(getTeacherId());





        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Student_Home.this,MainActivity.class);
                startActivity(intent);
            }
        });


    }

    private void generateTopic(Integer teacherId) {
        Integer id = teacherId;
        String url = getURL();
        String URL = url + "topic/getTopicByTeacherId?" + "id=" + id;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        topics = Topic.fromJson(response);
                        for(Topic topic : topics){
                            int wordsfinished = wordsFinished(topic.getName());
                            if (wordsfinished > 0.4 * topic.getWordCount()) unlockgame = true;
                            Log.d("this", "sssss:wordsfinished=  " + topic.getName()+ wordsfinished + "wordcount= "+ topic.getWordCount());
                        }

                        if(unlockgame == false){
                            game.setBackgroundResource(R.drawable.shapegrey);
                            game.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(Student_Home.this,"Complete one topic to unlock this!",Toast.LENGTH_SHORT).show();
                                }
                            });
                            trackProgress.setBackgroundResource(R.drawable.shapegrey);
                            trackProgress.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(Student_Home.this,"Complete one topic to unlock this!",Toast.LENGTH_SHORT).show();

                                }
                            });
                        } else {
                            game.setBackgroundResource(R.drawable.shapesignup);
                            game.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Student_Home.this,ViewTopic.class);
                                    intent.putExtra("game_page_start",1);
                                    startActivity(intent);
                                }
                            });
                            trackProgress.setBackgroundResource(R.drawable.shapesignup);
                            trackProgress.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Student_Home.this,TrackProgress2.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue queue = Volley.newRequestQueue(Student_Home.this);
        queue.add(jsonArrayRequest);
    }

    Integer wordsFinished(String topicName){
        Integer wordsFinished = 0;
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from Word where topic=?", new String[]{topicName});
        if(cursor.moveToFirst()){
            do{
                int activityFinished = 0;
                String rhyme = cursor.getString(cursor.getColumnIndex("rhyme"));
                String sentence = cursor.getString(cursor.getColumnIndex("sentence"));
                String category = cursor.getString(cursor.getColumnIndex("category"));
                String lookAndFeel = cursor.getString(cursor.getColumnIndex("lookAndFeel"));
                String association = cursor.getString(cursor.getColumnIndex("association"));
                String audioPath = cursor.getString(cursor.getColumnIndex("audioPath"));
                String wordAudioPath = cursor.getString(cursor.getColumnIndex("wordAudioPath"));
                String videoPath = cursor.getString(cursor.getColumnIndex("videoPath"));
                String photoPath = cursor.getString(cursor.getColumnIndex("photoPath"));
                int syllableFinished = cursor.getInt(cursor.getColumnIndex("syllableFinished"));
                if (!rhyme.isEmpty()) activityFinished++;
                if (!sentence.isEmpty()) activityFinished++;
                if (!category.isEmpty()) activityFinished++;
                if (!lookAndFeel.isEmpty()) activityFinished++;
                if (!association.isEmpty()) activityFinished++;
                if (!audioPath.isEmpty()) activityFinished++;
                if (!wordAudioPath.isEmpty()) activityFinished++;
                if (!videoPath.isEmpty()) activityFinished++;
                if (!photoPath.isEmpty()) activityFinished++;
                if (syllableFinished != 0) activityFinished++;
                if (activityFinished == 10){
                    wordsFinished++;
                }

            } while (cursor.moveToNext());

        }
        return wordsFinished;
    }

    private Integer getTeacherId() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Teacher_ID = "Key_TeacherId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer id = sharedPreferences.getInt(Teacher_ID,0);
        return id;
    }
    private String getURL() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL,null);
        return url;
    }

    //Creates channel for notification
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Chanel";
            String description = "";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("Notification", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    //Sends notification to alarm manager to schedule notification
    private void scheduleNotification(Notification notification, int delay) {
        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    //Creates notification with coin symbol
    private Notification getNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification");
        builder.setContentTitle("Word chest reminder");
        builder.setContentText("Want to collect more coins for your Word Chest? Keep practising your new words! ");
        builder.setSmallIcon(R.drawable.coin);
        builder.setChannelId("Notification");
        return builder.build();
    }


}
