package com.example.vocabapp.vocabapp.Entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Student {

    private Integer id;

    private String name;

    private String username;

    private String password;

    private Integer teacherId;

    private Integer wordCompleted;

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }


    public void setTeacherId(Integer teacherId) {

        this.teacherId = teacherId;
    }

    public Integer getTeacherId() {

        return teacherId;
    }

    public void setWordCompleted(Integer wordCompleted) {

        this.wordCompleted = wordCompleted;

    }

    public Integer getWordCompleted() {

        return wordCompleted;

    }

    //Convert from json object to java object
    public static Student fromJson(JSONObject jsonObject) {

        Student student = new Student();

        try {
            student.id = jsonObject.getInt("id");
            student.name = jsonObject.getString("name");
            student.username = jsonObject.getString("username");
            student.password = jsonObject.getString("password");
            student.teacherId = jsonObject.getInt("teacherId");
            student.wordCompleted = jsonObject.getInt("wordCompleted");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return student;
    }

    //Convert from json array to java array of objects
    public static ArrayList<Student> fromJson(JSONArray jsonArray) {

        JSONObject userJson;
        ArrayList<Student> students = new ArrayList<Student>(jsonArray.length());

        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                userJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Student student = Student.fromJson(userJson);
            if(student != null) {
                students.add(student);
            }
        }
        return students;

    }
}
