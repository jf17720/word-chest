package com.example.vocabapp.vocabapp.Model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Progress;
import com.example.vocabapp.vocabapp.Entity.Student;
import com.example.vocabapp.vocabapp.Entity.Teacher;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.StudentRegister;
import com.example.vocabapp.vocabapp.Teacher_Home;
import com.example.vocabapp.vocabapp.Tools.ProgressAdaptor;

import org.json.JSONArray;

import java.util.ArrayList;


public class StudentProgress extends AppCompatActivity {

    SharedPreferences userInfo;
    private static final String Teacher_ID = "Key_TeacherId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    private ListView listView;
    private ImageButton goBack;
    private ImageView toHome;
    ArrayList<Student> students = new ArrayList<>();
    ArrayList<Progress> progresses = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_progress);

        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        listView = findViewById(R.id.StudentPropgress_listView);
        goBack = findViewById(R.id.StudentPropgress_goBack);
        toHome = findViewById(R.id.StudentPropgress_goHome);

        Integer teacherId = userInfo.getInt(Teacher_ID,0);
        generateView generateView = new generateView();
        generateView.execute(teacherId);
        onClickListView();

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StudentProgress.this,Teacher_Home.class);
                startActivity(intent);
            }
        });
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StudentProgress.this,Teacher_Home.class);
                startActivity(intent);
            }
        });
    }

    //list all the students and show their progress
    private class generateView extends AsyncTask<Integer,Integer,Integer> {

        @Override
        protected Integer doInBackground(Integer... integers) {
            Integer id = integers[0];
            String url = userInfo.getString(BASIC_URL,null);
            String URL = url + "student/getStudentByTeacherId?" + "id=" + id;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            students = Student.fromJson(response);
                            LayoutInflater inflater = getLayoutInflater();
                            //the progress adaptor will align the words completed by the student and the total number of word
                            //that a student should complete, e.g: 2/10
                            ProgressAdaptor progressAdaptor = new ProgressAdaptor(inflater,students,getTotalWordCount());
                            listView.setAdapter(progressAdaptor);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            RequestQueue queue = Volley.newRequestQueue(StudentProgress.this);
            queue.add(jsonArrayRequest);
            return null;
        }
    }


    //This will go to the next page which show the progress of each topic
    private void onClickListView() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences;
                final String STUDENT_ID = "Key_StudentID";
                final String STUDENT_NAME = "Key_StudentName";
                final String PREFERENCES_STUDENT = "Preference_Student";
                sharedPreferences = getSharedPreferences(PREFERENCES_STUDENT,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(STUDENT_NAME,students.get(i).getName());
                editor.putInt(STUDENT_ID,students.get(i).getId());
                editor.apply();
                Intent intent = new Intent(StudentProgress.this,TopicProgress.class);
                startActivity(intent);
                finish();
            }
        });
    }

    //Get the total number of words associated to the student
    private Integer getTotalWordCount() {
        final String TotalWordCount = "Key_TotalWordCount";
        final String Preference_GetWordCount = "Preference_WordCount";
        SharedPreferences sharedPreferences = getSharedPreferences(Preference_GetWordCount,MODE_PRIVATE);
        Integer count = sharedPreferences.getInt(TotalWordCount,0);
        return count;
    }


//    private class deleteProgress extends AsyncTask<String,String,String> {
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String url = userInfo.getString(BASIC_URL,null);
//            String URL = url + "progress/delete";
//            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URL, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    Toast.makeText(StudentProgress.this, response, Toast.LENGTH_SHORT).show();
//                }
//            }, new Response.ErrorListener(){
//
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(StudentProgress.this, "Failed", Toast.LENGTH_SHORT).show();
//
//                }
//            });
//            RequestQueue queue = Volley.newRequestQueue(StudentProgress.this);
//            queue.add(stringRequest);
//            return null;
//        }
//    }


}

