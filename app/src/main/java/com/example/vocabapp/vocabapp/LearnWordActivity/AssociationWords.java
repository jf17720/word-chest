package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;

//Association words activity - students must write in a textbox any associations they can come up with regarding the given word
public class AssociationWords extends AppCompatActivity {


   private String content;
   private MyDatabaseHelper dbHelper; // database helper
   private String currentAssociation = "";
   private String BASIC_URL;
   private String theWord;
   private TextView showWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_association__words);

        theWord = intent.getStringExtra("the_word");
        showWord = findViewById(R.id.showWord_Association);
        showWord.setText(theWord);
        BASIC_URL = getURL();
        final TextView associationInput = (TextView) findViewById(R.id.editTextAssociation);
        String hint = String.format("What does %s make you think of?",theWord);
        associationInput.setHint(hint);

        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);

        final SQLiteDatabase db = dbHelper.getWritableDatabase(); //get database object

        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()}); //get cursor object
                                                                                                                                            //to read and write

        if(cursor.moveToFirst()){
            do{
                currentAssociation = cursor.getString(cursor.getColumnIndex("association"));
                if(currentAssociation != null) associationInput.setText(currentAssociation);
            } while (cursor.moveToNext());
            cursor.close();
        }

        //when back button is pressed, user must be returned to the previous page
        ImageButton backButton = (ImageButton) findViewById(R.id.backFromAssociation);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(AssociationWords.this, Learnwords.class);
                startActivity(intent);
            }

        });

        //when home button is pressed, user must be returned to the home page
        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromAssociation);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(AssociationWords.this, Student_Home.class);
                startActivity(intent);
            }

        });

        //when the save button is clicked, input must be saved to database and user must be returned to the learn words page
        final Button saveButton = (Button) findViewById(R.id.saveButtonAssociation);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                content= associationInput.getText().toString();
                //if the user has not input anything then set a message pop up to tell them the input cannot be empty
                if (content.matches("")){
                    Toast.makeText(AssociationWords.this, "INPUT CANNOT BE EMPTY", Toast.LENGTH_SHORT).show();

                }else { //if the user has input something then save it to the database
                    addToOnlineDatabase(content);
                    //create the database object
                    final CRUD_interface word = new DatabaseController_Word( v.getContext(),"WORD_STORE.db", null,1);
                    //select the row using word name and student ID
                    Word sele = word.select(theWord,getStudentID());
                    sele.setAssociation(content);
                    word.update(sele);
//                    Toast.makeText(AssociationWords.this, "Updated!", Toast.LENGTH_SHORT).show();
                    Log.d("database","updated:"+ sele.toString());
                    Toast.makeText(AssociationWords.this, "SAVED!", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(AssociationWords.this, Learnwords.class);
                    startActivity(intent);

                }

            }

        });
    }

    private Integer getStudentID() { // get student ID using stored sharedPreferences
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }



    private String getURL() { // get URL using stored sharedPreferences
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL, null);
        return url;
    }

    protected String addToOnlineDatabase(String updatedAssociation) { // update student input onto cloud database
        Integer StudentID = getStudentID();
        String URL = BASIC_URL + "progress/association?wordName=" + theWord + "&" + "studentId=" + StudentID + "&" + "association=" + updatedAssociation;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT,URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("Failed")) {
//                            Toast.makeText(AssociationWords.this,"failed",Toast.LENGTH_SHORT).show();
                        } else {
//                            Toast.makeText(AssociationWords.this, "added successfully!", Toast.LENGTH_SHORT).show();
//                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AssociationWords.this,"Request failed",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(AssociationWords.this);
        queue.add(stringRequest);
        return null;
    }

}
