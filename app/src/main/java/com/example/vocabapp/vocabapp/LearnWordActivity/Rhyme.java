package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;
import android.app.Activity;

public class Rhyme extends AppCompatActivity {
    public static int RhymeVisible = 0;
    private String content;
    private MyDatabaseHelper dbHelper;
    private String currentRhyme = "";
    private Boolean ifInputExisted = false;
    private String theWord;
    private String BASIC_URL;
    private TextView showWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rhyme);


        Intent intent = getIntent();
        theWord = intent.getStringExtra("the_word");
        showWord = findViewById(R.id.showWord_rhyme);
        showWord.setText(theWord);
        BASIC_URL = getURL();
        final TextView rhymeInput = (TextView) findViewById(R.id.editTextRhyme);

        String hint = String.format("What words rhyme with %s?",theWord);
        rhymeInput.setHint(hint);
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()});

        if(cursor.moveToFirst()){
            do{
                currentRhyme = cursor.getString(cursor.getColumnIndex("rhyme"));
                if(currentRhyme != null) rhymeInput.setText(currentRhyme);
            } while (cursor.moveToNext());
            cursor.close();
        }
//if the back button is clicked return the user to the learn words page
        ImageButton backButton = (ImageButton) findViewById(R.id.backFromRhyme);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Rhyme.this, Learnwords.class);
                startActivity(intent);
            }
        });

//if the home button is pressed return the user to the student home page
        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromRhyme);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Rhyme.this, Student_Home.class);
                startActivity(intent);
            }

        });

//when the save button is clicked, check whether input is empty , if not save to database and cause the icon to turn coloured
        Button saveButton = (Button) findViewById(R.id.saveButtonRhyme);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                content = rhymeInput.getText().toString();
//input cannot be empty
                if (content.matches("")){
                    Toast.makeText(Rhyme.this, "INPUT CANNOT BE EMPTY", Toast.LENGTH_SHORT).show();
                }else {
//                    let user know that their input has been saved
                        Toast.makeText(Rhyme.this, "SAVED!", Toast.LENGTH_SHORT).show();
//                        save to database
                        addToOnlineDatabase(content);
                        final CRUD_interface word = new DatabaseController_Word( v.getContext(),"WORD_STORE.db", null,1);
                            addToOnlineDatabase(content);
                            Word sele = word.select(theWord,getStudentID());
                            sele.setRhyme(content);
                            word.update(sele);
                            Toast.makeText(Rhyme.this, "Updated!", Toast.LENGTH_SHORT).show();
                            Log.d("database","updated:"+ sele.toString());
//                            turn the icon colloured
                        RhymeVisible = 1;
                        if(RhymeVisible != 1) {
                            Learnwords.green++;
                        }
//                        return user to previous page
                    Intent intent = new Intent(Rhyme.this, Learnwords.class);
                    startActivity(intent);

                }
            }

        });
    }

    private Integer getStudentID() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        final String Student_NAME = "Key_StudentName";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }

    protected String addToOnlineDatabase(String updatedRhyme) {
        Integer StudentID = getStudentID();
        String URL = BASIC_URL + "progress/rhyme?wordName=" + theWord + "&" + "studentId=" + StudentID + "&" + "rhyme=" + updatedRhyme;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT,URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("Failed")) {
//                            Toast.makeText(Rhyme.this,"failed",Toast.LENGTH_SHORT).show();
                        } else {
//                            Toast.makeText(Rhyme.this, "added successfully!", Toast.LENGTH_SHORT).show();
//                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Rhyme.this,"Request failed",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(Rhyme.this);
        queue.add(stringRequest);
        return null;
    }

    private String getURL() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL,null);
        return url;
    }

}
