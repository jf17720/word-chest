package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;

public class LookAndFeel extends AppCompatActivity {

    public static int LookAndFeelVisible = 0;
    private MyDatabaseHelper dbHelper;
    private String currentLookAndFeel = "";
    private String BASIC_URL;
    private String theWord;
    private String TAG="LookAndFeel";
    private TextView showWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_and_feel);

        Intent intent = getIntent();
        BASIC_URL = getURL();
        theWord = intent.getStringExtra("the_word");
        showWord = findViewById(R.id.showWord_lookAndFell);
        showWord.setText(theWord);
        final TextView lookAndFeelInput = (TextView) findViewById(R.id.editTextLookAndFeel);
        //select database
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);
        //get writable database object
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        //create cursor object to find row
        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()});
        //get the current stored input
        if(cursor.moveToFirst()){
            do{
                currentLookAndFeel = cursor.getString(cursor.getColumnIndex("lookAndFeel"));
                if(currentLookAndFeel != null) lookAndFeelInput.setText(currentLookAndFeel);
            } while (cursor.moveToNext());
            cursor.close();
        }


        String hint = String.format("What does %s look and feel like?",theWord);
        lookAndFeelInput.setHint(hint);


        //back button must return users to the learn words page.
        ImageButton backButton = (ImageButton) findViewById(R.id.backFromLookAndFeel);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LookAndFeel.this, Learnwords.class);
                startActivity(intent);
            }

        });

        //if the home button is clicked, must return user to the home page
        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromLookAndFeel);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LookAndFeel.this, Student_Home.class);
                startActivity(intent);
            }

        });

        //when the button is clicked get the text from the box and save it, produce a message saying that it has been saved.
        final Button saveButton = (Button) findViewById(R.id.saveButtonLookAndFeel);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String content = lookAndFeelInput.getText().toString();
                //if the input is empty, do not save or mark the activity is complete, include a pop up message to tell users that input cannot be empty
                if (content.matches("")){
                    Toast.makeText(LookAndFeel.this, "INPUT CANNOT BE EMPTY", Toast.LENGTH_SHORT).show();
                }else {
                    content= lookAndFeelInput.getText().toString();
                    if (content.matches("")){
                        Toast.makeText(LookAndFeel.this, "INPUT CANNOT BE EMPTY", Toast.LENGTH_SHORT).show();
                    }else { // get database table and update the current input

                        final CRUD_interface word = new DatabaseController_Word( v.getContext(),"WORD_STORE.db", null,1);
                        addToOnlineDatabase(content); // update the cloud databse
                        Word sele = word.select(theWord,getStudentID());
                        sele.setLookAndFeel(content);
                        word.update(sele);

//                        Toast.makeText(LookAndFeel.this, "Updated!", Toast.LENGTH_SHORT).show();
                        Log.d("database","updated:"+ sele.toString());
                        Toast.makeText(LookAndFeel.this, "SAVED!", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(LookAndFeel.this, Learnwords.class);
                        startActivity(intent);

                    }
                }

            }

        });
    }
    private Integer getStudentID() { //get student ID from stored sharedPreference
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }

    protected String addToOnlineDatabase(String updatedLookAndFeel) { //update student input onto cloud database
        Integer StudentID = getStudentID();
        String URL = BASIC_URL + "progress/lookAndFeel?wordName=" + theWord + "&" + "studentId=" + StudentID + "&" + "lookAndFeel=" + updatedLookAndFeel;
        Log.d(TAG, "addToOnlineDatabase: URL is: "+ URL);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT,URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("Failed")) {
//                            Toast.makeText(LookAndFeel.this,"failed",Toast.LENGTH_SHORT).show();
                        } else {
//                            Toast.makeText(LookAndFeel.this, "added successfully!", Toast.LENGTH_SHORT).show();
//                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LookAndFeel.this,"Request failed",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(LookAndFeel.this);
        queue.add(stringRequest);
        return null;
    }

    private String getURL() { //get URL from sharedPreference
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL,null);
        return url;
    }

}
