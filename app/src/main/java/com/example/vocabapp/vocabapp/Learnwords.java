package com.example.vocabapp.vocabapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Progress;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.LearnWordActivity.AssociationWords;
import com.example.vocabapp.vocabapp.LearnWordActivity.LookAndFeel;
import com.example.vocabapp.vocabapp.LearnWordActivity.RecordAction;
import com.example.vocabapp.vocabapp.LearnWordActivity.RecordWord;
import com.example.vocabapp.vocabapp.LearnWordActivity.Rhyme;
import com.example.vocabapp.vocabapp.LearnWordActivity.Sentence;
import com.example.vocabapp.vocabapp.LearnWordActivity.Syllables;
import com.example.vocabapp.vocabapp.LearnWordActivity.TakePhoto;
import com.example.vocabapp.vocabapp.LearnWordActivity.WriteCategory;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Model.ViewWord;
import com.example.vocabapp.vocabapp.LearnWordActivity.RecordInitialSoundActivity;
import com.example.vocabapp.vocabapp.Model.WordProgress;
//import com.example.vocabapp.vocabapp.recordingForWord.RecordWordActivity;

import org.json.JSONArray;

import java.util.ArrayList;


public class Learnwords extends AppCompatActivity {

    public static int green = 0;
    public static int yellow = 0;
    public static int red = 0;
    private static final String PREFERENCE_VIEWTOPIC = "Preference_ViewTopic";
    private static final String PROGRESS = "Key_Process";
    private static final String UPDATE_FLAG = "UpdateState";
    private static final String PREFERENCE_UPDATE = "Preference_Update";
    final String TOPIC_NAME = "Key_TopicName";
    final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
    final String TOPIC_ID = "Key_TopicId";
    public ImageButton rhymeButton, categoryButton, cameraButton, associationButton, LookFeelButton, homeButton, sentenceButton, recordWordButton, RecordActionButton, syllablesButton, initialSoundButton;
    private String TAG = "Learnwords";
    MyDatabaseHelper dbHelper;
    String wordName;
    Boolean flag = true;
    private int numOfActivityCompleted = 0;
    int numOfWordCompleted = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learnwords);
        rhymeButton = (ImageButton) findViewById(R.id.rhymeButton);
        categoryButton = (ImageButton) findViewById(R.id.CategoryButton);
        cameraButton = (ImageButton) findViewById(R.id.camera);
        associationButton = (ImageButton) findViewById(R.id.thoughtBubble);
        LookFeelButton = (ImageButton) findViewById(R.id.touch);
        homeButton = (ImageButton) findViewById(R.id.homeFromLearnWords);
        sentenceButton = (ImageButton) findViewById(R.id.sentenceButton);
        recordWordButton = (ImageButton) findViewById(R.id.megaphone);
        RecordActionButton = (ImageButton) findViewById(R.id.Recordaction);
        syllablesButton = (ImageButton) findViewById(R.id.syllables);
        initialSoundButton = (ImageButton) findViewById(R.id.initialSoundButton);


        displayWord();
        getWordCompleted();
        SharedPreferences preferences = getSharedPreferences(PREFERENCE_UPDATE, MODE_PRIVATE);
        CreateProgress createProgress = new CreateProgress();
        createProgress.execute(getURL());
//        updateWordCompleted updateWordCompleted = new updateWordCompleted();
//        updateWordCompleted.execute();


        final CRUD_interface CreateWord = new DatabaseController_Word(this, "WORD_STORE.db", null, 1);
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null, 1);
        ArrayList wordsInterted = new ArrayList();
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{wordName, getStudentId().toString()});
        Word currentWord = new Word();
        if (cursor.moveToFirst()) {
            do {
                String word = cursor.getString(cursor.getColumnIndex("word"));
                wordsInterted.add(word);
            } while (cursor.moveToNext());
        }

        if (wordsInterted.contains(wordName)) {
            if (cursor.moveToFirst()) {
                do {
                    String rhyme = cursor.getString(cursor.getColumnIndex("rhyme"));
                    String sentence = cursor.getString(cursor.getColumnIndex("sentence"));
                    String category = cursor.getString(cursor.getColumnIndex("category"));
                    String lookAndFeel = cursor.getString(cursor.getColumnIndex("lookAndFeel"));
                    String association = cursor.getString(cursor.getColumnIndex("association"));
                    String audioPath = cursor.getString(cursor.getColumnIndex("audioPath"));
                    String wordAudioPath = cursor.getString(cursor.getColumnIndex("wordAudioPath"));
                    String videoPath = cursor.getString(cursor.getColumnIndex("videoPath"));
                    String photoPath = cursor.getString(cursor.getColumnIndex("photoPath"));
                    int syllableFinished = cursor.getInt(cursor.getColumnIndex("syllableFinished"));
                    currentWord.setRhyme(rhyme);
                    currentWord.setSentence(sentence);
                    currentWord.setCategory(category);
                    currentWord.setLookAndFeel(lookAndFeel);
                    currentWord.setAssociation(association);
                    currentWord.setAudioPath(audioPath);
                    currentWord.setWordAudioPath(wordAudioPath);
                    currentWord.setVideoPath(videoPath);
                    currentWord.setPhotoPath(photoPath);
                    currentWord.setSyllableFinished(syllableFinished);
                } while (cursor.moveToNext());
            }

            //check whether the activities have been completed before, if they have make the icon colourful if they haven't then keep them grey
            if (!currentWord.getRhyme().equals("")) {
                rhymeButton.setImageResource(R.drawable.rhymebutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getAudioPath().equals("")) {
                initialSoundButton.setImageResource(R.drawable.soundbutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getAssociation().equals("")) {
                associationButton.setImageResource(R.drawable.yellowcloudbutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getSyllableFinished().equals(0)) {
                syllablesButton.setImageResource(R.drawable.clappingbutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getCategory().equals("")) {
                categoryButton.setImageResource(R.drawable.tagbutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getWordAudioPath().equals("")) {
                recordWordButton.setImageResource(R.drawable.redmegaphonebutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getPhotoPath().equals("")) {
                cameraButton.setImageResource(R.drawable.redcamerabutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getVideoPath().equals("")) {
                RecordActionButton.setImageResource(R.drawable.videobutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getLookAndFeel().equals("")) {
                LookFeelButton.setImageResource(R.drawable.clapbutton);
                numOfActivityCompleted++;
            }
            if (!currentWord.getSentence().equals("")) {
                sentenceButton.setImageResource(R.drawable.sentencebutton);
                numOfActivityCompleted++;
            }
        }

        //num == 10 means all the activities are completed so the state of the progress of the word is updated to 1
        if(numOfActivityCompleted == 10) {
            isCompleted isCompleted = new isCompleted();
            isCompleted.execute();
            numOfActivityCompleted = 0;
        }

        Log.d(TAG, "onCreate: student_id= " + getStudentId());

        if (!wordsInterted.contains(wordName)) {
            Word newWord = new Word();
            newWord.setName(wordName);
            newWord.setStd_id(getStudentId());
            newWord.setTopic(getTopicInfo().getName());
            newWord.setRhyme("");
            newWord.setCategory("");
            newWord.setSentence("");
            newWord.setAssociation("");
            newWord.setLookAndFeel("");
            newWord.setPhotoPath("");
            newWord.setAudioPath("");
            newWord.setVideoPath("");
            newWord.setWordAudioPath("");
            newWord.setSyllableFinished(0);
            CreateWord.insert(newWord);
        } else {
//            Word word = CreateWord.select(wordName);
//            if(flag) {
//                if(checkIfCompleted(word)){
//                    DoProgressUpdate progressUpdate = new DoProgressUpdate();
//                    progressUpdate.execute(getStudentId().toString());
//                    flag = false;
//                }
//            }
        }
        //if camera button is clicked: send user to take photo page
        cameraButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, TakePhoto.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }
        });

        recordWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Learnwords.this, RecordWord.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }
        });

        //if record word button is clicked: send user to record word page
        recordWordButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, RecordWord.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });
        //if syllables button is clicked: send user to syllables page
        syllablesButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, Syllables.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });

        //if look and feel button is clicked: send user to look and feel page page
        LookFeelButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, LookAndFeel.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });

        //if record action button is clicked: send user to record action page
        RecordActionButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, RecordAction.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });

        //if sentence recording button is clicked: send user to sentence page
        sentenceButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, Sentence.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });

        //if association button is clicked: send user to association page
        associationButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, AssociationWords.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });

        //if back button is clicked: send user to pick a word page
        final ImageButton backButton = (ImageButton) findViewById(R.id.backFromLearnWords);
        backButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(
                        Learnwords.this, ViewWord.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });

        //if home button is clicked: send user to student home page
        homeButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, Student_Home.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });


        //if rhyme button is clicked: send user to rhyme page
        rhymeButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, Rhyme.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });


        //if inital sound button is clicked: send user to inital sound page
        initialSoundButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, RecordInitialSoundActivity.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });

        //if category button is clicked: send user to category page
        categoryButton.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, WriteCategory.class);
                intent.putExtra("the_word", wordName);
                startActivity(intent);
            }

        });
    }

    //check if a word has been completed before
    private Boolean checkIfCompleted(Word word) {
        if (word.getRhyme() != null && word.getSyllableFinished() != null && word.getCategory() != null && word.getLookAndFeel() != null
                && word.getAssociation() != null && word.getSentence() != null) {
            return true;
        }
        return false;
    }


//    set the title to be the word that they have picked to learn
    private void displayWord() {
        SharedPreferences sharedPreferences;
        final String WORD_ID = "Key_WordId";
        final String WORD_NAME = "Key_WordName";
        final String PREFERENCE_WORD = "Preference_Word";
        sharedPreferences = getSharedPreferences(PREFERENCE_WORD, MODE_PRIVATE);
        final Integer wordID = sharedPreferences.getInt(WORD_ID, 0);
        final String wordName = sharedPreferences.getString(WORD_NAME, null);
        this.wordName = sharedPreferences.getString(WORD_NAME, null);
        TextView textView = findViewById(R.id.wordToLearn);
        textView.setText(wordName);
    }

//    retrieve the ID of the student
    private Integer getStudentId() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        final String Student_NAME = "Key_StudentName";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        Integer id = sharedPreferences.getInt(Student_ID, 0);
        return id;
    }


    private Topic getTopicInfo() {
        final String TOPIC_ID = "Key_TopicId";
        final String TOPIC_NAME = "Key_TopicName";
        final String PREFERENCES_VIEWTOPIC = "Preference_ViewTopic";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_VIEWTOPIC, MODE_PRIVATE);
        Topic topic = new Topic();
        String name = sharedPreferences.getString(TOPIC_NAME, null);
        Integer id = sharedPreferences.getInt(TOPIC_ID, 0);
        topic.setName(name);
        topic.setId(id);
        return topic;
    }

    private String getURL() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String BASIC_URL = "Key_Url";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        String url = sharedPreferences.getString(BASIC_URL, null);
        return url;
    }

    private Integer getTeacherId() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Teacher_ID = "Key_TeacherId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        Integer id = sharedPreferences.getInt(Teacher_ID, 0);
        return id;
    }


    //create the progress in the database
    private class CreateProgress extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String basic_URL = strings[0];
            Integer studentId = getStudentId();
            String topicName = getTopicInfo().getName();
            Integer topicId = getTopicInfo().getId();
            Integer teacherId = getTeacherId();
            String URL = basic_URL + "progress/add?" + "wordName=" + wordName + "&" + "studentId=" + studentId + "&" + "topic=" +
                    topicName + "&" + "topicId=" + topicId + "&" + "teacherId=" + teacherId;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Toast.makeText(Learnwords.this, response, Toast.LENGTH_SHORT).show();
                            SharedPreferences preferences = getSharedPreferences(PREFERENCE_UPDATE, MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(PROGRESS, wordName);
                            editor.putBoolean(UPDATE_FLAG, true);
                            editor.commit();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(Learnwords.this,"Request failed",Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue queue = Volley.newRequestQueue(Learnwords.this);
            queue.add(stringRequest);
            return null;
        }
    }


    //called when all the activites of the word is completed by the student
    private class isCompleted extends AsyncTask<String, String, String> {
        protected String doInBackground(String... strings) {
            String basic_URL = getURL();
            String URL = basic_URL + "progress/complete?" + "wordName=" + wordName + "&" + "studentId=" + getStudentId() + "&" +
                    "completed=" + 1;
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Learnwords.this, "Request failed", Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue queue = Volley.newRequestQueue(Learnwords.this);
            queue.add(stringRequest);
            return null;
        }
    }

    //Calculate the number of word completed from the progress table
    private void getWordCompleted() {
        String basic_URL = getURL();
        String URL = basic_URL + "progress/getProgressByStudent?" + "id=" + getStudentId();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
//                        Toast.makeText(Learnwords.this, "numofProgress=" + response.length(), Toast.LENGTH_SHORT).show();
                        ArrayList<Progress> progresses = Progress.fromJson(response);
                        int num = 0;
                        for(Progress progress : progresses) {
                            if(progress.getComplete() == 1) {
                                numOfWordCompleted++;
                            }
                        }
                        updateWordCompleted updateWordCompleted = new updateWordCompleted();
                        updateWordCompleted.execute();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Learnwords.this, "Request failed", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rQueue = Volley.newRequestQueue(Learnwords.this);
        rQueue.add(jsonArrayRequest);
    }



    //Update the number of word completed by the student
    private class updateWordCompleted extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String basic_URL = getURL();
            String URL = basic_URL + "student/updateWordCompleted?" + "id=" + getStudentId() + "&" + "number=" + numOfWordCompleted;
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Learnwords.this, "Request failed", Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue queue = Volley.newRequestQueue(Learnwords.this);
            queue.add(stringRequest);
            return null;
        }
    }
}











