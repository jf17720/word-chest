package com.example.vocabapp.vocabapp.LearnWordActivity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vocabapp.vocabapp.Learnwords;
import com.example.vocabapp.vocabapp.LocalDB.CRUD_interface;
import com.example.vocabapp.vocabapp.LocalDB.DatabaseController_Word;
import com.example.vocabapp.vocabapp.LocalDB.MyDatabaseHelper;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Student_Home;

import java.io.IOException;
import java.util.UUID;

public class RecordInitialSoundActivity extends AppCompatActivity {
    public static int InitialSoundVisible = 0;


    private ImageButton simpleRecord;
    private ImageButton simplePlay;
    private TextView playText;
    private TextView recordText;
    private TextView showWord;
    private ImageButton goback, toHome;
    private boolean ifStarted = true;
    private boolean ifPaused = true;
    private MyDatabaseHelper dbHelper;
    String pathSave = "";
    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;
    String theWord;



    final int REQUEST_PERMISSION_CODE = 1000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_initial_sound);
        Intent intent = getIntent();
        theWord = intent.getStringExtra("the_word");
        showWord = findViewById(R.id.showWord_InitialSound);
        showWord.setText(theWord);
        dbHelper = new MyDatabaseHelper(this, "WORD_STORE.db", null,1);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from Word where word=? and std_id=?", new String[]{theWord,getStudentID().toString()});

        if(cursor.moveToFirst()){
            do{
                pathSave = cursor.getString(cursor.getColumnIndex("audioPath"));
            } while (cursor.moveToNext());
            cursor.close();
        }

        simplePlay = (ImageButton) findViewById(R.id.PlayButton2);
        simpleRecord = (ImageButton) findViewById(R.id.RecordButton2);
        playText = findViewById(R.id.textView15);
        recordText = findViewById(R.id.textView14);
        goback = findViewById(R.id.backFromRecordInitialSound);
        toHome = findViewById(R.id.homeFromRecordInitialSound);
        if(!checkPermissionFromDevice()){
            requestPermission();
        }

        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecordInitialSoundActivity.this,Student_Home.class);
                startActivity(intent);
            }
        });

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecordInitialSoundActivity.this,Learnwords.class);
                startActivity(intent);
            }
        });

        simpleRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecord(ifStarted);
                ifStarted = !ifStarted;

            }
        });

        simplePlay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onPlay(ifPaused);
                ifPaused = !ifPaused;
            }
        });


    }

    private void onRecord(boolean start){
        if(start){
            startRecord();
            simpleRecord.setImageResource(R.drawable.stoprecordbtn);
            recordText.setText("Tap to stop");
        } else {
            stopRecord();
            simpleRecord.setImageResource(R.drawable.recordbtn);
            recordText.setText("Tap to Record");

        }
    }

    private void onPlay(boolean playback){
        if(playback){
            startPlayBack();
            simplePlay.setImageResource(R.drawable.stopplayingbtn);
            playText.setText("Tap to stop");
        } else {
            simplePlay.setImageResource(R.drawable.playbutton);
            stopPlayBack();
            playText.setText("Tap to play");
        }
    }

    private void startRecord(){
        pathSave = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/"
                + UUID.randomUUID().toString()+"_acdiorecord.3gp";
        setupMediaRecorder();

        try{
            mediaRecorder.prepare();
            mediaRecorder.start();
            final CRUD_interface word = new DatabaseController_Word( this,"WORD_STORE.db", null,1);
            Word sele = word.select(theWord,getStudentID());
            sele.setAudioPath(pathSave);
            word.update(sele);
            Log.d("database","updated:"+ sele.toString());
            }catch(IOException e){
            e.printStackTrace();
            }
            simplePlay.setEnabled(false);
            Toast.makeText(RecordInitialSoundActivity.this, "recording...",Toast.LENGTH_SHORT).show();
    }

    private void stopRecord(){
        mediaRecorder.stop();
        simplePlay.setEnabled(true);
        simpleRecord.setEnabled(true);
        Toast.makeText(RecordInitialSoundActivity.this, "stopped...",Toast.LENGTH_SHORT).show();
    }

    private void startPlayBack(){
        simpleRecord.setEnabled(false);

        mediaPlayer = new MediaPlayer();
        try{
            mediaPlayer.setDataSource(pathSave);
            mediaPlayer.prepare();

        } catch (IOException e){
            e.printStackTrace();
        }

        mediaPlayer.start();
        Toast.makeText(RecordInitialSoundActivity.this,"playing...",Toast.LENGTH_SHORT).show();
    }

    private void stopPlayBack(){
        simpleRecord.setEnabled(true);
        simpleRecord.setEnabled(true);

        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            setupMediaRecorder();
        }
    }
    private void setupMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(pathSave);
    }

    private  boolean checkPermissionFromDevice(){
        int wirte_external_storge_result = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio_result = ContextCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO);
        boolean ext_storage = (wirte_external_storge_result == PackageManager.PERMISSION_GRANTED);
        boolean rec_audio = (record_audio_result == PackageManager.PERMISSION_DENIED);
        return rec_audio && ext_storage;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        switch (requestCode){
            case REQUEST_PERMISSION_CODE: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT);
            }
        }
    }

    private Integer getStudentID() {
        final String PREFERENCES_StoreId = "Preference_Id";
        final String Student_ID = "Key_StudentId";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        Integer ID = sharedPreferences.getInt(Student_ID,0);
        return ID;
    }
}
