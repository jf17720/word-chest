package com.example.vocabapp.vocabapp.Model;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.vocabapp.vocabapp.Entity.Student;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.StudentRegister;
import com.example.vocabapp.vocabapp.Teacher_Home;

import org.json.JSONArray;

import java.util.ArrayList;

public class ViewStudent extends AppCompatActivity {

    SharedPreferences userInfo;
    private static final String Teacher_ID = "Key_TeacherId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    private static final String TAG = "ViewTopic";
    SwipeMenuListView listView;
    ImageButton goBack;
    ImageView toHome,addStudent;
    ArrayList<Student> students = new ArrayList<>();
    ArrayList<String> list = new ArrayList<>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);
        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);
        listView = findViewById(R.id.ViewStudent_listView);
        goBack = findViewById(R.id.ViewStudent_goBack);
        toHome = findViewById(R.id.ViewStudent_goHome);
        addStudent = findViewById(R.id.ViewStudent_addStudent);
        Integer teacherId = userInfo.getInt(Teacher_ID,0);
        generateListView(teacherId);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewStudent.this,Teacher_Home.class);
                startActivity(intent);
            }
        });
        addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewStudent.this,StudentRegister.class);
                startActivity(intent);
            }
        });
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewStudent.this,Teacher_Home.class);
                startActivity(intent);
            }
        });


        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(220);
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete_);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        listView.setMenuCreator(creator);
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            String url = userInfo.getString(BASIC_URL, null);
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
                        Integer studentId = students.get(position).getId();
                        Log.d(TAG, "onMenuItemClick: position: "+ position);
                        Log.d(TAG, "onMenuItemClick: student id: "+ studentId);
                        String URL_deleteStudent = url + "student/deleteById?id=" + studentId;
                        String URL_deleteProgress = url + "progress/deleteByStudentId?id=" + studentId;
                        deleteProgress deleteProgress = new deleteProgress();
                        deleteProgress.execute(URL_deleteProgress);

                        RequestQueue queue = Volley.newRequestQueue(ViewStudent.this);
                        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URL_deleteStudent,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        if (response.equals("Failed")) {
                                            Toast.makeText(ViewStudent.this, "Delete student failed", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(ViewStudent.this, "Delete", Toast.LENGTH_SHORT).show();
                                            list.remove(position);
                                            students.remove(position);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(ViewStudent.this, "Request delete student failed", Toast.LENGTH_SHORT).show();

                            }
                        });
                        queue.add(stringRequest);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
    }

    //After deleting the student the progress by the student is deleted accordingly
    private class deleteProgress extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, strings[0],
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.equals("Failed")) {
                                Toast.makeText(ViewStudent.this, "Delete progress failed", Toast.LENGTH_SHORT).show();
                            } else {

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            RequestQueue queue = Volley.newRequestQueue(ViewStudent.this);
            queue.add(stringRequest);
            return null;
        }
    }


   //Getting all the student associated to the teacher and display on the listview
    private void generateListView(Integer teacherId) {
        Integer id = teacherId;
        String url = userInfo.getString(BASIC_URL,null);
        String URL = url + "student/getStudentByTeacherId?" + "id=" + id;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        students = Student.fromJson(response);
                        for(Student student : students) {
                            list.add(student.getName());
                        }
                        adapter = new ArrayAdapter<String>(ViewStudent.this,R.layout.customlayout,R.id.CustomTextView,list);
                        listView.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue queue = Volley.newRequestQueue(ViewStudent.this);
        queue.add(jsonArrayRequest);
    }

}
