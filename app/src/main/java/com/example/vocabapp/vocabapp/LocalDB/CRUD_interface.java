package com.example.vocabapp.vocabapp.LocalDB;
import com.example.vocabapp.vocabapp.Entity.Word;

public interface CRUD_interface {      // interface for 4 basic database operations
    public boolean insert(Word word);
    public boolean delete(int id);
    public boolean update(Word word);
    public Word select(String theword,Integer std_id);
}
