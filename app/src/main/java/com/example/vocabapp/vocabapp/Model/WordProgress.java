package com.example.vocabapp.vocabapp.Model;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Progress;
import com.example.vocabapp.vocabapp.Entity.Topic;
import com.example.vocabapp.vocabapp.Entity.Word;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Teacher_Home;
import com.example.vocabapp.vocabapp.Tools.WordProgressAdaptor;

import org.json.JSONArray;

import java.util.ArrayList;

public class WordProgress extends AppCompatActivity {

    SharedPreferences userInfo;
    private static final String Teacher_ID = "Key_TeacherId";
    private static final String Identity = "Key_Identity";
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    private ListView listView;
    private ImageButton goBack;
    private ImageView toHome;
    private ArrayList<Word> words;
    private ArrayList<Progress> progresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_progress);

        listView = findViewById(R.id.WordProgress_listView);
        goBack = findViewById(R.id.WordProgress_goBack);
        toHome = findViewById(R.id.WordProgress_goHome);
        userInfo = getSharedPreferences(PREFERENCES_StoreId, MODE_PRIVATE);
        getProgressByStudentId();
        generateView generateView = new generateView();
        generateView.execute(getTopicID().toString());
        onClickListView();

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WordProgress.this,TopicProgress.class);
                startActivity(intent);
            }
        });
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WordProgress.this,Teacher_Home.class);
                startActivity(intent);
            }
        });
    }


    private Integer getTopicID() {
        final String TOPIC_ID = "Key_TopicId";
        final String PREFERENCES_TopicProgress = "Preference_ViewTopic";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_TopicProgress,MODE_PRIVATE);
        return sharedPreferences.getInt(TOPIC_ID,0);
    }

    private Integer getStudentID() {
        final String STUDENT_ID = "Key_StudentID";
        final String PREFERENCES_TopicProgress = "Preference_ViewTopic";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_TopicProgress,MODE_PRIVATE);
        return sharedPreferences.getInt(STUDENT_ID,0);
    }

    private String getTopicName() {
        final String TOPIC_NAME = "Key_TopicName";
        final String PREFERENCES_TopicProgress = "Preference_ViewTopic";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_TopicProgress,MODE_PRIVATE);
        return sharedPreferences.getString(TOPIC_NAME,null);
    }


    //list all the words and show that if the word is completed or not
    private class generateView extends AsyncTask<String,String,String> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(WordProgress.this,"Loading","Please wait...",false,false);
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            //Dismissing the progress dialog
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(String... strings) {
            Integer id = Integer.valueOf(strings[0]);
            String url = userInfo.getString(BASIC_URL,null);
            String URL = url + "word/getWordByTopicId?" + "id=" + id;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            words = Word.fromJson(response);
                            LayoutInflater inflater = getLayoutInflater();
                            WordProgressAdaptor wordProgressAdaptor = new WordProgressAdaptor(inflater,words,progresses,getStudentID());
                            listView.setAdapter(wordProgressAdaptor);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            RequestQueue queue = Volley.newRequestQueue(WordProgress.this);
            queue.add(jsonArrayRequest);
            return null;
        }
    }

    private void onClickListView() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences;
                final String Word_Name = "Key_WordName";
                final String PREFERENCES_WordProgress = "Preference_WordProgress";
                sharedPreferences = getSharedPreferences(PREFERENCES_WordProgress,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                String word_Name = words.get(i).getName();
                editor.putString(Word_Name,word_Name);
                editor.apply();
                Intent intent = new Intent(WordProgress.this,WordInput.class);
                startActivity(intent);
            }
        });
    }


    private void getProgressByStudentId() {
        String url = userInfo.getString(BASIC_URL,null);
        String URL = url + "/progress/getProgressByStudent?id=" + getStudentID();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progresses = Progress.fromJson(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue rQueue = Volley.newRequestQueue(WordProgress.this);
        rQueue.add(jsonArrayRequest);
    }


}
