package com.example.vocabapp.vocabapp.Model;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.vocabapp.vocabapp.Entity.Progress;
import com.example.vocabapp.vocabapp.R;
import com.example.vocabapp.vocabapp.Teacher_Home;

import org.json.JSONObject;

public class WordInput extends AppCompatActivity {

    SharedPreferences userInfo;
    ImageButton goBack;
    ImageView toHome;
    private static final String BASIC_URL = "Key_Url";
    private static final String PREFERENCES_StoreId = "Preference_Id";
    Progress progress;
    String TAG = "WordInput.class";
    private TextView rhyme,rhymeContent;
    private TextView syllable,syllableContent;
    private TextView lookAndFeel,lookAndFeelContent;
    private TextView association,associationContent;
    private TextView category,categoryContent;
    private TextView sentence,sentenceContent;
    private TextView display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_input);

//        listView = findViewById(R.id.WordInput_listView);
        goBack = findViewById(R.id.WordInput_goBack);
        toHome = findViewById(R.id.WordInput_toHome);
        display = findViewById(R.id.WordInput_display);
        rhyme = findViewById(R.id.InputTextView_Rhyme);
        rhymeContent = findViewById(R.id.InputTextView_RhymeContent);
        syllable = findViewById(R.id.InputTextView_Syllable);
        syllableContent = findViewById(R.id.InputTextView_SyllableContent);
        category = findViewById(R.id.InputTextView_Category);
        categoryContent = findViewById(R.id.InputTextView_CategoryContent);
        lookAndFeel = findViewById(R.id.InputTextView_Look);
        lookAndFeelContent = findViewById(R.id.InputTextView_LookContent);
        association = findViewById(R.id.InputTextView_Association);
        associationContent = findViewById(R.id.InputTextView_AssociationContent);
        sentence = findViewById(R.id.InputTextView_Sentence);
        sentenceContent = findViewById(R.id.InputTextView_SentenceContent);
        userInfo = getSharedPreferences(PREFERENCES_StoreId,MODE_PRIVATE);

        rhyme.setText("Rhyme:");
        syllable.setText("Syllables:");
        category.setText("Category:");
        lookAndFeel.setText("LookAndFeel:");
        association.setText("Association:");
        sentence.setText("Sentence:");
        display.setText(getWordName().trim());

        sentence.setVisibility(View.INVISIBLE);
        sentenceContent.setVisibility(View.INVISIBLE);



        generateView();

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WordInput.this,WordProgress.class);
                startActivity(intent);
            }
        });
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WordInput.this,Teacher_Home.class);
                startActivity(intent);
            }
        });

    }

    private Integer getStudentID() {
        final String STUDENT_ID = "Key_StudentID";
        final String PREFERENCES_TopicProgress = "Preference_ViewTopic";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_TopicProgress,MODE_PRIVATE);
        return sharedPreferences.getInt(STUDENT_ID,0);
    }

    private String getWordName() {
        final String Word_Name = "Key_WordName";
        final String PREFERENCES_WordProgress = "Preference_WordProgress";
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_WordProgress,MODE_PRIVATE);
        return sharedPreferences.getString(Word_Name,null);
    }


    //list the word and all the attributes with student input
    private void generateView() {
        String url = userInfo.getString(BASIC_URL,null);
        String URL = url + "/progress/getProgressByStudentAndWordName?" + "id=" + getStudentID() + "&" + "name=" + getWordName();
        JsonObjectRequest jsonObjectRequest  = new JsonObjectRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress = Progress.fromJson(response);
                        //the defaut value is Not Completed which is changed only when the student has completed the attribute
                        if(!progress.getRhyme().equalsIgnoreCase("")) {
                            rhymeContent.setText(progress.getRhyme().trim());
                        }
                        if(!progress.getSyllableFinished().toString().equalsIgnoreCase("")) {
                            syllableContent.setText(progress.getSyllableFinished().toString().trim());
                        }
                        if(!progress.getCategory().equalsIgnoreCase("")) {
                            categoryContent.setText(progress.getCategory().trim());
                        }
                        if(!progress.getLookAndFeel().equalsIgnoreCase("")) {
                            lookAndFeelContent.setText(progress.getLookAndFeel().trim());
                        }
                        if(!progress.getAssociation().equalsIgnoreCase("")) {
                            associationContent.setText(progress.getAssociation().trim());
                        }
                        if(!progress.getSentence().equalsIgnoreCase("")) {
                            sentenceContent.setText(progress.getSentence().trim());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(WordInput.this, "request failed", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rQueue = Volley.newRequestQueue(WordInput.this);
        rQueue.add(jsonObjectRequest);

    }

}
