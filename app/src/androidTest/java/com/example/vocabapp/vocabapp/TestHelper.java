package com.example.vocabapp.vocabapp;

import android.support.test.espresso.ViewInteraction;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public class TestHelper {
    static public void LoginStudent(){
        ViewInteraction appCompatEditText = onView(withId(R.id.et_uname));
        appCompatEditText.perform(click());

        ViewInteraction appCompatEditText2 = onView(withId(R.id.et_uname));
        appCompatEditText2.perform(replaceText("ft1234"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(withId(R.id.et_pass));
        appCompatEditText3.perform(replaceText("123456"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(withId(R.id.et_pass));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(withId(R.id.Btn_student_signin));
        appCompatButton.perform(click());
    }


}
