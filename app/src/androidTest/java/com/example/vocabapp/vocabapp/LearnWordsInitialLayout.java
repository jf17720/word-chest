package com.example.vocabapp.vocabapp;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withResourceName;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
//
//Tests that the only the green activities e.g. Rhyme, Syllable and Initial Sound are displayed and that none of the red or orange
//
//
public class LearnWordsInitialLayout {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void LearnWordLayoutTest() {
        TestHelper.LoginStudent();

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.StudentHome_viewTopic),
                        isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction appCompatButton3 = onView(
                allOf(withText("Fruit"),
                        isDisplayed()));
        appCompatButton3.perform(click());

        ViewInteraction appCompatButton4 = onView(
                allOf(withText("apple"),
                        isDisplayed()));
        appCompatButton4.perform(click());

        ViewInteraction imageButton = onView(withId(R.id.rhymeButton));
        imageButton.check(matches(isDisplayed()));

        ViewInteraction imageButton2 = onView(withId(R.id.initialSoundButton));
        imageButton2.check(matches(isDisplayed()));

        ViewInteraction imageButton3 = onView(withId(R.id.syllables));
        imageButton3.check(matches(isDisplayed()));

        ViewInteraction imageButton5 = onView(withId(R.id.megaphone));
        imageButton5.check(matches(isDisplayed()));

        ViewInteraction imageButton6 = onView(withId(R.id.sentenceButton));
        imageButton6.check(matches(isDisplayed()));

        ViewInteraction imageButton7 = onView(withId(R.id.CategoryButton));
        imageButton7.check(matches(isDisplayed()));

        ViewInteraction imageButton8 = onView(withId(R.id.touch));
        imageButton8.check(matches(isDisplayed()));

        ViewInteraction imageButton9 = onView(withId(R.id.thoughtBubble));
        imageButton9.check(matches(isDisplayed()));

        ViewInteraction imageButton4 = onView(withId(R.id.camera));
        imageButton4.check(matches(isDisplayed()));

        ViewInteraction imageButton10 = onView(withId(R.id.Recordaction));
        imageButton10.check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

}
